/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* HTTP client and server primitives for embedded use                        */
/*****************************************************************************/
#include "http_agent.h"
#include "../common/common_utilities.h"

#ifndef WIN32
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#define closesocket close
#else
#include <Windows.h>
typedef int socklen_t;
#endif

#ifdef WIN32
const std::string TEMPORARY_FOLDER = "C:\\Componentality\\Temporary";
const std::string HTTP_ROOT_FOLDER = "C:\\Componentality\\HTTP";
#else
const std::string HTTP_ROOT_FOLDER = "/var/componentality/http";
const std::string TEMPORARY_FOLDER = "/tmp";
#endif

const std::string CST::HTTP::HTTP_ERROR_404 = "404 NOT FOUND";
const std::string CST::HTTP::HTTP_OK = "200 OK";
const std::string CST::HTTP::HTTP_1_0 = "HTTP/1.0";
const std::string CST::HTTP::HTTP_1_1 = "HTTP/1.1";

const std::string CST::HTTP::MIME_APP_ATOM = "application/atom+xml";
const std::string CST::HTTP::MIME_APP_X12 = "application/EDI-X12";
const std::string CST::HTTP::MIME_APP_EDIFACT = "application/EDIFACT";
const std::string CST::HTTP::MIME_APP_JSON = "application/json";
const std::string CST::HTTP::MIME_APP_JAVASCRIPT = "application/javascript";
const std::string CST::HTTP::MIME_APP_BIN = "application/octet-stream";
const std::string CST::HTTP::MIME_APP_OGG = "application/ogg";
const std::string CST::HTTP::MIME_APP_PDF = "application/pdf";
const std::string CST::HTTP::MIME_APP_POSTSCRIPT = "application/postscript";
const std::string CST::HTTP::MIME_APP_SOAP = "application/soap+xml";
const std::string CST::HTTP::MIME_APP_FONT = "application/font-woff";
const std::string CST::HTTP::MIME_APP_XHTML = "application/xhtml+xml";
const std::string CST::HTTP::MIME_APP_DTD = "application/xml-dtd";
const std::string CST::HTTP::MIME_APP_XOP = "application/xop+xml";
const std::string CST::HTTP::MIME_APP_ZIP = "application/zip";
const std::string CST::HTTP::MIME_APP_GZIP = "application/gzip";
const std::string CST::HTTP::MIME_APP_TORRENT = "application/x-bittorrent";
const std::string CST::HTTP::MIME_APP_TEXT = "application/x-tex";

const std::string CST::HTTP::MIME_AUDIO_BASIC = "audio/basic";
const std::string CST::HTTP::MIME_AUDIO_L24 = "audio/L24";
const std::string CST::HTTP::MIME_AUDIO_MP4 = "audio/mp4";
const std::string CST::HTTP::MIME_AUDIO_AAC = "audio/aac";
const std::string CST::HTTP::MIME_AUDIO_MPEG = "audio/mpeg";
const std::string CST::HTTP::MIME_AUDIO_OGG = "audio/ogg";
const std::string CST::HTTP::MIME_AUDIO_VORBIS = "audio/vorbis";
const std::string CST::HTTP::MIME_AUDIO_WMA = "audio/x-ms-wma";
const std::string CST::HTTP::MIME_AUDIO_WMA_REDIRECT = "audio/x-ms-wax";
const std::string CST::HTTP::MIME_AUDIO_REALAUDIO = "audio/vnd.rn-realaudio";
const std::string CST::HTTP::MIME_AUDIO_WMV = "audio/vnd.wave";
const std::string CST::HTTP::MIME_AUDIO_WEBM = "audio/webm";

const std::string CST::HTTP::MIME_IMAGE_GIF = "image/gif";
const std::string CST::HTTP::MIME_IMAGE_JPEG = "image/jpeg";
const std::string CST::HTTP::MIME_IMAGE_PJPEG = "image/pjpeg";
const std::string CST::HTTP::MIME_IMAGE_PNG = "image/png";
const std::string CST::HTTP::MIME_IMAGE_BMP = "image/x-windows-bmp";
const std::string CST::HTTP::MIME_IMAGE_SVG = "image/svg+xml";
const std::string CST::HTTP::MIME_IMAGE_TIFF = "image/tiff";
const std::string CST::HTTP::MIME_IMAGE_ICO = "image/vnd.microsoft.icon";
const std::string CST::HTTP::MIME_IMAGE_WBMP = "image/vnd.wap.wbmp";
const std::string CST::HTTP::MIME_IMAGE_WEBP = "image/webp";

const std::string CST::HTTP::MIME_MSG_HTTP = "message/http";
const std::string CST::HTTP::MIME_MSG_IMDN = "message/imdn+xml";
const std::string CST::HTTP::MIME_MSG_PARTIAL = "message/partial";
const std::string CST::HTTP::MIME_MSG_RFC822 = "message/rfc822";

const std::string CST::HTTP::MIME_TEXT_CMD = "text/cmd";
const std::string CST::HTTP::MIME_TEXT_CSS = "text/css";
const std::string CST::HTTP::MIME_TEXT_CSV = "text/csv";
const std::string CST::HTTP::MIME_TEXT_HTML = "text/html";
const std::string CST::HTTP::MIME_TEXT_JAVASCRIPT = "text/javascript";
const std::string CST::HTTP::MIME_TEXT_PLAIN = "text/plain";
const std::string CST::HTTP::MIME_TEXT_PHP = "text/php";
const std::string CST::HTTP::MIME_TEXT_XML = "text/xml";

const std::string CST::HTTP::MIME_VIDEO_MPEG1 = "video/mpeg";
const std::string CST::HTTP::MIME_VIDEO_MPEG4 = "video/mp4";
const std::string CST::HTTP::MIME_VIDEO_OGG = "video/ogg";
const std::string CST::HTTP::MIME_VIDEO_QUICKTIME = "video/quicktime";
const std::string CST::HTTP::MIME_VIDEO_WEBM = "video/webm";
const std::string CST::HTTP::MIME_VIDEO_WMV = "video/x-ms-wmv";
const std::string CST::HTTP::MIME_VIDEO_FLV = "video/x-flv";
const std::string CST::HTTP::MIME_VIDEO_3GPP = "video/3gpp";
const std::string CST::HTTP::MIME_VIDEO_3GPP2 = "video/3gpp2";

using namespace CST::HTTP;

struct EXT_MIME_DESCRIPTION
{
	std::string mExtension;
	std::string mMime;
};

EXT_MIME_DESCRIPTION __ext_mime_mapping[] =
{
	{ "json", MIME_APP_JSON },
	{ "pdf", MIME_APP_PDF },
	{ "ps", MIME_APP_POSTSCRIPT },
	{ "zip", MIME_APP_ZIP },
	{ "gz", MIME_APP_GZIP },
	{ "pcm", MIME_AUDIO_BASIC },
	{ "aac", MIME_AUDIO_AAC },
	{ "mp3", MIME_AUDIO_MPEG },
	{ "wmv", MIME_AUDIO_WMV },
	{ "gif", MIME_IMAGE_GIF },
	{ "jpg", MIME_IMAGE_JPEG },
	{ "jpeg", MIME_IMAGE_JPEG },
	{ "png", MIME_IMAGE_PNG },
	{ "bmp", MIME_IMAGE_BMP },
	{ "svg", MIME_IMAGE_SVG },
	{ "tiff", MIME_IMAGE_TIFF },
	{ "ico", MIME_IMAGE_ICO },
	{ "webp", MIME_IMAGE_WEBP },
	{ "css", MIME_TEXT_CSS },
	{ "csv", MIME_TEXT_CSV },
	{ "htm", MIME_TEXT_HTML },
	{ "html", MIME_TEXT_HTML },
	{ "js", MIME_TEXT_JAVASCRIPT },
	{ "xml", MIME_TEXT_XML },
	{ "txt", MIME_TEXT_PLAIN },
	{ "mpeg", MIME_VIDEO_MPEG1 },
	{ "mpg", MIME_VIDEO_MPEG4 },
	{ "mp4", MIME_VIDEO_MPEG4 },
	{ "ogg", MIME_VIDEO_OGG },
	{ "mov", MIME_VIDEO_QUICKTIME },
	{ "webm", MIME_VIDEO_WEBM },
	{ "wmv", MIME_VIDEO_WMV },
	{ "flv", MIME_VIDEO_FLV },
	{ "3gpp", MIME_VIDEO_3GPP },
	{ "", "" }
};

#ifndef WIN32

void CST::HTTP::isolate_sigpipe(void) {
	sigset_t sigpipe_mask;
	sigemptyset(&sigpipe_mask);
	sigaddset(&sigpipe_mask, SIGPIPE);
	sigset_t saved_mask;
	if (pthread_sigmask(SIG_BLOCK, &sigpipe_mask, &saved_mask) == -1)
		perror("pthread_sigmask");
}

#else

void CST::HTTP::isolate_sigpipe(void) {}

#endif

using namespace CST::HTTP;

void CST::HTTP::init()
{
#ifdef WIN32
	{
		WSADATA wsaData;
		WSAStartup(MAKEWORD(2, 2), &wsaData);
	}
#endif
}

std::string CST::HTTP::get(const std::string host, const std::string address, const int port)
{
	char buf[20];
	sprintf(buf, "%d", port);
	EasyHTTPClient::HEADERS headers;
	std::string content;
	EasyHTTPClient::RESPONSE result = EasyHTTPClient::request(std::string("http://") + host + ':' + buf + address + (address.empty() ? "/" : ""), headers, content);
	if (!result.mAvailable || (result.mResponseCode == 599))
		return "!";
	else
		return result.mContent;
}

EasyHTTPServer::EasyHTTPServer(const int port, const int maxconn, const int expiration)
	: TCPServer(this, port, maxconn), mKeepAliveExpiration(expiration) {};

EasyHTTPServer::~EasyHTTPServer() {};

void EasyHTTPServer::onError(const int64_t& uid)
{
}
	
void EasyHTTPServer::onStop(const int64_t& uid)
{

}

void EasyHTTPServer::onReceive(const int64_t& uid, CST::Common::blob* data)
{
	std::string request = CST::Common::blobToString(*data);
	std::vector<std::string> headers;
	std::string cheader;
	for (size_t i = 0; i < request.length(); i++)
	{
		if (cheader.empty() && (request[i] <= ' ')) continue;
		if (request[i] == '\r') continue;
		if (request[i] == '\n')
		{
			headers.push_back(cheader);
			cheader.clear();
			continue;
		}
		cheader.push_back(request[i]);
	}
	if (!cheader.empty())
		headers.push_back(cheader);
	// Find Connection header and mark connection as keepalive if needed
	mLock.lock();
	mKeepAlive[uid] = false;
	mConnectionAccessTime[uid] = CST::Common::getSecondsCounter();
	mLock.unlock();
	const std::string tofind = CST::Common::lowcase("Connection:");
	for (std::vector<std::string>::iterator i = headers.begin(); i != headers.end(); i++)
	{
		if (CST::Common::lowcase(i->substr(0, tofind.size())) == tofind)
		{
			std::string conntype = i->substr(tofind.size());
			conntype = CST::Common::lowcase(CST::Common::trim(conntype));
			if ((conntype == "keep-alive") && (mKeepAliveExpiration))
			{
				mLock.lock();
				mKeepAlive[uid] = true;
				mLock.unlock();
			}
			break;
		}
	}

	processRequest(uid, headers);
}

void EasyHTTPServer::onSend(const int64_t& uid, const size_t remain)
{
	CST::Common::scoped_lock lock(protect_connections);
	mProcessedConnections[uid] = uid;
	mConnectionAccessTime[uid] = CST::Common::getSecondsCounter();
}

void EasyHTTPServer::onConnect(const int64_t& uid)
{
	closeExpiredConnections();
}
	
void EasyHTTPServer::onDisconnect(const int64_t& uid)
{
	CST::Common::scoped_lock lock(protect_connections);
	mProcessedConnections[uid] = uid;
}

void EasyHTTPServer::processRequest(const int64_t& uid, const std::vector<std::string>& headers)
{
	std::vector<std::string>& _headers = (std::vector<std::string>&) headers;
	std::pair<std::string, std::string> response_desc;
	for (std::vector<std::string>::iterator i = _headers.begin(); i != _headers.end(); i++)
		if (i->find("GET ") == 0)
		{
			std::string resource_name;
			for (size_t idx = 4; ((idx < i->length()) && ((*i)[idx] > ' ')); idx++)
				resource_name += (*i)[idx];
			response_desc = getResource(resource_name, headers);
			std::string response;
			if (response_desc.second.find('\n') != ____UNDEFINED)
				response = response_desc.second;
			else
				response = composeResponse(HTTP_1_1, HTTP_OK, response_desc.second);
			char buf[20];
			sprintf(buf, "%d", response_desc.first.length());
			response += "\r\nContent-Length: " + std::string(buf);
			response += "\r\n\r\n" + response_desc.first;
			CST::Common::blob blob = CST::Common::stringToBlob(response);
			send(uid, blob);
		}
}

std::pair<std::string, std::string> EasyHTTPServer::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	return std::pair<std::string, std::string>("<http><body>Error 404<br>Componentality<br>Common Libs<br>HTTP Agent<br>Server not configured or unsupported URL</body></http>", 
		composeResponse(HTTP_1_0, HTTP_ERROR_404, "text/html"));
}

std::string EasyHTTPServer::composeResponse(const std::string& http_version,
	const std::string& response,
	const std::string& mime_type,
	const std::string charset,
	const std::string additional_info)
{
	std::string result = http_version + ' ' + response + "\r\nContent-Type: " + mime_type;
	if (charset.empty())
		result += "; charset=utf-8";
	else
		result += "; charset = " + charset;
	return result;
}

void EasyHTTPServer::closeConnection(const int64_t& uid)
{
	{
		CST::Common::scoped_lock lock(mLock);
		if (mConnectionAccessTime.find(uid) != mConnectionAccessTime.end())
			mConnectionAccessTime.erase(mConnectionAccessTime.find(uid));
		if (mKeepAlive.find(uid) != mKeepAlive.end())
			mKeepAlive.erase(mKeepAlive.find(uid));
	}
	TCPServer::closeConnection(uid);
}

void EasyHTTPServer::closeExpiredConnections()
{
	int64_t time = CST::Common::getSecondsCounter();
	std::list<int64_t> ids;
	{
		CST::Common::scoped_lock lock(protect_connections);
		for (std::map<int64_t, int64_t>::iterator i = mProcessedConnections.begin(); i != mProcessedConnections.end(); i++)
			ids.push_back(i->first);
	}
	CST::Common::scoped_lock lock(mLock);
	for (std::list<int64_t>::iterator i = ids.begin(); i != ids.end(); i++)
	{
		int64_t uid = *i;
		if (mKeepAlive.find(uid) == mKeepAlive.end())
		{
			closeConnection(uid);
			{
				CST::Common::scoped_lock lock(protect_connections);
				if (mProcessedConnections.find(uid) != mProcessedConnections.end())
					mProcessedConnections.erase(mProcessedConnections.find(uid));
			}
			continue;
		}
		else if (mKeepAlive.find(uid) != mKeepAlive.end())
			if (mKeepAlive[uid])
				if (mConnectionAccessTime.find(uid) != mConnectionAccessTime.end())
					if (time - mConnectionAccessTime[uid] > mKeepAliveExpiration)
					{
						mKeepAlive.erase(mKeepAlive.find(uid));
						closeConnection(uid);
						{
							CST::Common::scoped_lock lock(protect_connections);
							if (mProcessedConnections.find(uid) != mProcessedConnections.end())
								mProcessedConnections.erase(mProcessedConnections.find(uid));
						}
					}
	}
}

//////////////////////////////////////////////////////////////////////////////////////

HTTPServer::HTTPServer(const int port, const int maxconn) : EasyHTTPServer(port, maxconn, 0), running(false) {}

HTTPServer::~HTTPServer() {}

void HTTPServer::subscribe(HTTPResourceProvider& rp)
{
	while (true)
	{
		{
			CST::Common::scoped_lock lock(mProvidersLock);
			if (!running)
				break;
		}
		CST::Common::sleep(50);
	}
	if (mProviders.find(&rp) == mProviders.end())
		mProviders[&rp] = &rp;
}

void HTTPServer::unsubscribe(HTTPResourceProvider& rp)
{
	while (true)
	{
		CST::Common::scoped_lock lock(mProvidersLock);
		if (!running)
			break;
	}
	if (mProviders.find(&rp) != mProviders.end())
		mProviders.erase(mProviders.find(&rp));
}

std::pair<std::string, std::string> HTTPServer::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	{
		CST::Common::scoped_lock lock(mProvidersLock);
		running = true;
	}
	for (std::map<HTTPResourceProvider*, HTTPResourceProvider*>::iterator i = mProviders.begin(); i != mProviders.end(); i++)
	{
		std::pair<std::string, std::string> result = i->second->getResource(address, headers);
		if (!result.second.empty())
		{
			{
				CST::Common::scoped_lock lock(mProvidersLock);
				running = false;
			}
			return result;
		}
	}
	{
		CST::Common::scoped_lock lock(mProvidersLock);
		running = false;
	}
	return EasyHTTPServer::getResource(address, headers);
}

//////////////////////////////////////////////////////////////////////////////////////////////

HTTPResourceProvider::PARSED_URL HTTPResourceProvider::parseAddress(const std::string& address)
{
	HTTPResourceProvider::PARSED_URL result;
	size_t pos = 0;
	bool parse_key = true;
	std::string key;
	std::string value;

	for (; pos < address.size(); pos++)
	{
		if (address[pos] == '?')
			break;
		else
			result.first.push_back(address[pos]);
	}

	pos += 1;

	for (; pos < address.size(); pos++)
	{
		if (parse_key)
			if (address[pos] == '=')
				parse_key = false;
			else
				key.push_back(address[pos]);
		else
			if (address[pos] == '&')
			{
				parse_key = true;
				result.second[key] = value;
				key.clear();
				value.clear();
			}
			else
				value.push_back(address[pos]);

	}
	if (!key.empty())
		result.second[key] = value;
	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

EasyHTTPClient::RESPONSE EasyHTTPClient::request(const std::string& _address, HEADERS& headers, const std::string& content, const std::string type, const int _port, const int timeout)
{
	int port = _port;
	RESPONSE response;
	response.mResponseCode = 599;
	response.mAvailable = false;
	ADDRESS url = parse(_address);
	if (!url.mPort.empty())
		sscanf((char*)url.mPort.c_str(), "%d", &port);

	struct timeval tv;
	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	isolate_sigpipe();

	int socket_type = SOCK_STREAM;
	int socket_proto = IPPROTO_TCP;
	int sock = socket(PF_INET, socket_type, socket_proto);
	setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(struct timeval));
	int on = 1;

	if (sock < 0)
		return response;

	mRqLock.lock();
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	hostent* hostaddr;
	char* ip;
	hostaddr = gethostbyname(url.mHost.c_str());
	if (!hostaddr || !hostaddr->h_addr_list)
	{
		mRqLock.unlock();
		closesocket(sock);
		return response;
	}
	ip = inet_ntoa(*(struct in_addr *)*hostaddr->h_addr_list);
	mRqLock.unlock();

	// Set up the sockaddr structure
	addr.sin_addr.s_addr = inet_addr(ip);
	addr.sin_port = htons(port);

	int result = connect(sock, (const sockaddr*)&addr, sizeof(addr));
	if (result != 0)
	{
		closesocket(sock);
		return response;
	}

	std::string header = std::string() + type + ' ' + url.mAddress + " HTTP/1.1" + "\r\n";
	for (HEADERS::iterator i = headers.begin(); i != headers.end(); i++)
		header += *i + "\r\n";
	char buf[20];
	sprintf(buf, "%d", content.size());
	header += std::string("Content-Length: ") + buf + "\r\n";
	header += "Host: " + url.mHost + "\r\n\r\n";
	header += content;

	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval)) < 0)
	{
		closesocket(sock);
		return response;
	}

	result = send(sock, header.c_str(), header.size(), 0);

	CST::Common::sleep(500);
	char rec;

	unsigned long long start = CST::Common::getTimeMilliSec();
	int recsize;
	do
	{
		recsize = recv(sock, &rec, 1, 0);
		if (recsize > 0)
			response.mContent += rec;
	} while ((recsize > 0) || (CST::Common::getTimeMilliSec() - start < (timeout * 500 / 3)));

	closesocket(sock);
	response.mAvailable = !response.mContent.empty();
	parseResponse(response);
	return response;
}

EasyHTTPClient::ADDRESS EasyHTTPClient::parse(const std::string& address)
{
	ADDRESS result;
	size_t pos;
	std::string tail = address;
	pos = tail.find("://");
	if (pos != ____UNDEFINED)
	{
		result.mProtocol = tail.substr(0, pos);
		tail = tail.substr(pos + 3);
	}
	pos = tail.find("@");
	if (pos != ____UNDEFINED)
	{
		std::string psp = tail.substr(0, pos);
		tail = tail.substr(pos + 1);
		pos = psp.find(':');
		if (pos != ____UNDEFINED)
		{
			result.mLogin = psp.substr(0, pos);
			result.mPassword = psp.substr(pos + 1);
		}
		else
			result.mLogin = psp;
	}
	pos = tail.find('/');
	if (pos != ____UNDEFINED)
	{
		result.mHost = tail.substr(0, pos);
		tail = tail.substr(pos);
	}
	else
	{
		result.mHost = tail;
		tail.clear();
	}
	result.mAddress = tail;
	pos = result.mHost.find(':');
	if (pos != ____UNDEFINED)
	{
		result.mPort = result.mHost.substr(pos + 1);
		result.mHost = result.mHost.substr(0, pos);
	}
	return result;
}

void EasyHTTPClient::parseResponse(RESPONSE& result)
{
	std::string header;
	for (size_t i = 0; i < result.mContent.size(); i++)
		if (result.mContent[i] != '\r')
			header += result.mContent[i];
	result.mContent.clear();
	size_t pos = header.find("\n\n");
	if (pos != ____UNDEFINED)
	{
		result.mContent = header.substr(pos + 2);
		header = header.substr(0, pos);
	}
	pos = header.find('\n');
	if (pos != ____UNDEFINED)
	{
		std::string tail = header.substr(pos + 1);
		header = header.substr(0, pos);
		while (true)
		{
			pos = tail.find('\n');
			if (pos != ____UNDEFINED)
			{
				result.mHeaders.push_back(tail.substr(0, pos));
				tail = tail.substr(pos + 1);
			}
			else
			{
				if (!tail.empty())
					result.mHeaders.push_back(tail);
				break;
			}
		}
	}
	pos = header.find(' ');
	if (pos != ____UNDEFINED)
	{
		result.mProtocolVersion = header.substr(0, pos);
		header = header.substr(pos + 1);
	}
	pos = header.find(' ');
	if (pos != ____UNDEFINED)
	{
		std::string rsp = header.substr(0, pos);
		result.mResponseInfo = header.substr(pos + 1);
		sscanf(rsp.c_str(), "%d", &result.mResponseCode);
	}
}

CST::Common::mutex EasyHTTPClient::mRqLock;

//////////////////////////////////////////////////////////////////////////////////////////

FileSystemAccessProvider::FileSystemAccessProvider(HTTPServer& server, const std::string root) : mServer(server), mRootFolder(root)
{
	if (mRootFolder.empty())
		mRootFolder = HTTP_ROOT_FOLDER;
	mServer.subscribe(*this);
}

FileSystemAccessProvider::~FileSystemAccessProvider()
{
	mServer.unsubscribe(*this);
}

std::pair<std::string, std::string> FileSystemAccessProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	std::pair<std::string, std::string> result;
	std::string _address = address;
	if ((_address.size() > 0) && (_address[0] == '/'))
		_address.erase(0, 1);
	std::string fname = CST::Common::fileJoinPaths(mRootFolder, _address);
	if (!CST::Common::fileExists(fname))
		return result;
	CST::Common::blob file = CST::Common::fileRead(fname);
	if (file.mSize)
	{
		result.first = CST::Common::blobToString(file);
		result.second = MIME_APP_BIN;
	}
	file.purge();
	return result;
}

/////////////////////////////////////////////////////////////////////////////////////////

FileAccessProvider::FileAccessProvider(HTTPServer& server, const std::string root) : FileSystemAccessProvider(server, root)
{
	size_t i = 0;
	while (!__ext_mime_mapping[i].mMime.empty())
	{
		mMime[__ext_mime_mapping[i].mExtension] = __ext_mime_mapping[i].mMime;
		i++;
	}
}

FileAccessProvider::~FileAccessProvider()
{

}

std::pair<std::string, std::string> FileAccessProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	std::pair<std::string, std::string> result = FileSystemAccessProvider::getResource(address, headers);
	if (!result.second.empty())
	{
		size_t pos = ____UNDEFINED;
		for (size_t i = address.size(); i > 0; i--)
			if (address[i - 1] == '.')
			{
				pos = i - 1;
				break;
			}
		if (pos != ____UNDEFINED)
		{
			std::string ext = address.substr(pos + 1);
			if (mMime.find(ext) != mMime.end())
				result.second = mMime[ext];
		}
	}
	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::string TEMP_NAME_PREFIX = "httpTemp_";
std::string TEMP_NAME_EXT = ".tmp";
std::string CGI_BIN_FOLDER = "cgi-bin";

struct CGI_DESCRIPTOR
{
	char* mExtension;
	char* mExecutable;
} __cgi_desc[] =
{
	{"py", "python"},
	{NULL, NULL}
};

CGIProvider::CGIProvider(HTTPServer& server, const std::string root) : mServer(server), mRootFolder(root)
{
	if (mRootFolder.empty())
		mRootFolder = CST::Common::fileJoinPaths(HTTP_ROOT_FOLDER, CGI_BIN_FOLDER);
	std::list<std::string> tmp_files = CST::Common::listFiles(mRootFolder);
	// Find and remove all temporary files from a previous session
	for (std::list<std::string>::iterator i = tmp_files.begin(); i != tmp_files.end(); i++)
	{
		std::string prefix = i->substr(0, TEMP_NAME_PREFIX.size());
		if (prefix == TEMP_NAME_PREFIX)
			::remove(CST::Common::fileJoinPaths(mRootFolder, *i).c_str());
	}
	mTempNameCounter = 0;
	mServer.subscribe(*this);
}

CGIProvider::~CGIProvider()
{
	mServer.unsubscribe(*this);
}

std::pair<std::string, std::string> CGIProvider::getResource(const std::string& address, const std::vector<std::string>& headers)
{
	std::pair<std::string, std::string> result;
	char findex[20];
	sprintf(findex, "%d", mTempNameCounter++);

	std::string tmp_filename = CST::Common::fileJoinPaths(mRootFolder, TEMP_NAME_PREFIX + findex + TEMP_NAME_EXT);
	std::string content;
	for (size_t i = 0; i < headers.size(); i++)
	{
		if (!content.empty())
			content += '\n';
		content += headers[i];
	}
	CST::Common::fileWrite(tmp_filename, CST::Common::stringToBlob(content));

	return result;
}
