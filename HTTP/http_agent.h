/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* HTTP client and server primitives for embedded use                        */
/*****************************************************************************/
#ifndef HTTP_AGENT_H
#define HTTP_AGENT_H

#include <string>
#include <vector>
#include <map>
#include "../network/nettcp.h"

namespace CST
{
	namespace HTTP
	{

		extern const std::string HTTP_ERROR_404;			// HTTP Error 404
		extern const std::string HTTP_OK;					// HTTP OK
		extern const std::string HTTP_1_0;					// HTTP version 1.0
		extern const std::string HTTP_1_1;					// HTTP version 1.1

		extern const std::string MIME_APP_ATOM;
		extern const std::string MIME_APP_X12;
		extern const std::string MIME_APP_EDIFACT;
		extern const std::string MIME_APP_JSON;
		extern const std::string MIME_APP_JAVASCRIPT;
		extern const std::string MIME_APP_BIN;
		extern const std::string MIME_APP_OGG;
		extern const std::string MIME_APP_PDF;
		extern const std::string MIME_APP_POSTSCRIPT;
		extern const std::string MIME_APP_SOAP;
		extern const std::string MIME_APP_FONT;
		extern const std::string MIME_APP_XHTML;
		extern const std::string MIME_APP_DTD;
		extern const std::string MIME_APP_XOP;
		extern const std::string MIME_APP_ZIP;
		extern const std::string MIME_APP_GZIP;
		extern const std::string MIME_APP_TORRENT;
		extern const std::string MIME_APP_TEXT;

		extern const std::string MIME_AUDIO_BASIC;
		extern const std::string MIME_AUDIO_L24;
		extern const std::string MIME_AUDIO_MP4;
		extern const std::string MIME_AUDIO_AAC;
		extern const std::string MIME_AUDIO_MPEG;
		extern const std::string MIME_AUDIO_OGG;
		extern const std::string MIME_AUDIO_VORBIS;
		extern const std::string MIME_AUDIO_WMA;
		extern const std::string MIME_AUDIO_WMA_REDIRECT;
		extern const std::string MIME_AUDIO_REALAUDIO;
		extern const std::string MIME_AUDIO_WMV;
		extern const std::string MIME_AUDIO_WEBM;

		extern const std::string MIME_IMAGE_GIF;
		extern const std::string MIME_IMAGE_JPEG;
		extern const std::string MIME_IMAGE_PJPEG;
		extern const std::string MIME_IMAGE_PNG;
		extern const std::string MIME_IMAGE_BMP;
		extern const std::string MIME_IMAGE_SVG;
		extern const std::string MIME_IMAGE_TIFF;
		extern const std::string MIME_IMAGE_ICO;
		extern const std::string MIME_IMAGE_WBMP;
		extern const std::string MIME_IMAGE_WEBP;

		extern const std::string MIME_MSG_HTTP;
		extern const std::string MIME_MSG_IMDN;
		extern const std::string MIME_MSG_PARTIAL;
		extern const std::string MIME_MSG_RFC822;

		extern const std::string MIME_TEXT_CMD;
		extern const std::string MIME_TEXT_CSS;
		extern const std::string MIME_TEXT_CSV;
		extern const std::string MIME_TEXT_HTML;
		extern const std::string MIME_TEXT_JAVASCRIPT;
		extern const std::string MIME_TEXT_PLAIN;
		extern const std::string MIME_TEXT_PHP;
		extern const std::string MIME_TEXT_XML;

		extern const std::string MIME_VIDEO_MPEG1;
		extern const std::string MIME_VIDEO_MPEG4;
		extern const std::string MIME_VIDEO_OGG;
		extern const std::string MIME_VIDEO_QUICKTIME;
		extern const std::string MIME_VIDEO_WEBM;
		extern const std::string MIME_VIDEO_WMV;
		extern const std::string MIME_VIDEO_FLV;
		extern const std::string MIME_VIDEO_3GPP;
		extern const std::string MIME_VIDEO_3GPP2;

		void init();
		std::string get(const std::string host, const std::string address, const int port = 80);
		extern void isolate_sigpipe(void);

		struct RESOURCE_DESC
		{
			bool mAvailable;
			std::string mContent;
			std::string mMimeType;
			std::string mHeaders;
			RESOURCE_DESC() {mAvailable = false;}
		};

		class EasyHTTPServer : public CST::Network::TCPServer, public CST::Network::NetEventHandler
		{
		protected:
			std::map<int64_t, int64_t> mProcessedConnections;
			std::map<int64_t, int64_t> mConnectionAccessTime;			// Timestamp when connection accessed
			std::map<int64_t, bool> mKeepAlive;							// True if connection is keepalive connection
			int64_t                 mKeepAliveExpiration;				// Keepalive connection expiration (seconds)
			CST::Common::mutex mLock;
		public:
			EasyHTTPServer(const int port = 80, const int maxconn = 10, const int expiration = 10);
			virtual ~EasyHTTPServer();
		public:
			virtual int64_t getKeepAliveSessionTimeout() const { return mKeepAliveExpiration; }
			virtual void setKeepAliveSessionTimeout(const int64_t time) { mKeepAliveExpiration = time; }
		protected:
			virtual void onError(const int64_t& uid);				    // Called on error
			virtual void onStop(const int64_t& uid);					// Called when data successfully sent
			virtual void onReceive(const int64_t& uid, CST::Common::blob* data);				// Called when new data arrived
			virtual void onSend(const int64_t& uid, const size_t);					// Called when data successfully sent
			virtual void onConnect(const int64_t& uid);				    // Called when link is established
			virtual void onDisconnect(const int64_t& uid);			    // Called when link is ended
		protected:
			virtual void processRequest(const int64_t& uid, const std::vector<std::string>& headers);
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
			virtual void closeConnection(const int64_t& uid);
			virtual void closeExpiredConnections();
		protected:
			virtual std::string composeResponse(const std::string& http_version,
				const std::string& response,
				const std::string& mime_type,
				const std::string charset = "",
				const std::string additional_info = "");
		};

		class HTTPServer;

		class HTTPResourceProvider
		{
			friend class HTTPServer;
		public:
			typedef std::pair<std::string, std::map<std::string, std::string> > PARSED_URL;
		protected:
			virtual PARSED_URL parseAddress(const std::string& address);
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers) = 0;
		};

		class FileSystemAccessProvider : public HTTPResourceProvider
		{
		protected:
			std::string mRootFolder;
			HTTPServer& mServer;
		public:
			FileSystemAccessProvider(HTTPServer& server, const std::string root = "");
			virtual ~FileSystemAccessProvider();
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

		class FileAccessProvider : public FileSystemAccessProvider
		{
		protected:
			std::map<std::string, std::string> mMime;
		public:
			FileAccessProvider(HTTPServer& server, const std::string root = "");
			virtual ~FileAccessProvider();
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

		class CGIProvider : public HTTPResourceProvider
		{
		protected:
			std::string mRootFolder;
			HTTPServer& mServer;
			int mTempNameCounter;
		public:
			CGIProvider(HTTPServer& server, const std::string root = "");
			virtual ~CGIProvider();
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

		class HTTPServer : public EasyHTTPServer
		{
		protected:
			std::map<HTTPResourceProvider*, HTTPResourceProvider*> mProviders;
			CST::Common::mutex mProvidersLock;
			bool running;
		public:
			HTTPServer(const int port = 80, const int maxconn = 30);
			virtual ~HTTPServer();
			virtual void subscribe(HTTPResourceProvider&);
			virtual void unsubscribe(HTTPResourceProvider&);
		protected:
			virtual std::pair<std::string, std::string> getResource(const std::string& address, const std::vector<std::string>& headers);
		};

		class EasyHTTPClient
		{
		protected:
			static CST::Common::mutex mRqLock;
		public:
			typedef std::list<std::string> HEADERS;
			struct RESPONSE
			{
				bool mAvailable;
				HEADERS mHeaders;
				int mResponseCode;
				std::string mResponseInfo;
				std::string mProtocolVersion;
				std::string mContent;
			};
			struct ADDRESS
			{
				std::string mProtocol;
				std::string mHost;
				std::string mLogin;
				std::string mPassword;
				std::string mPort;
				std::string mAddress;
			};
		protected:
			EasyHTTPClient() {};
			virtual ~EasyHTTPClient() {};
		protected:
			static void parseResponse(RESPONSE& result);
		public:
			static ADDRESS  parse(const std::string& address);
			static RESPONSE request(const std::string& address, HEADERS& headers, const std::string& content, std::string type = "GET", const int port = 80, const int timeout = 3);
		};

	}
}


#endif