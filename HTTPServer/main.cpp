/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* Example. Easy embedded HTTP server                                        */
/*****************************************************************************/
#include <stdio.h>
#include <string>

#include "../common/common_utilities.h"
#include "../HTTP/http_agent.h"

#ifndef WIN32
#include<unistd.h>
#endif
using namespace CST::Common;

int main(int argc, char* argv[])
{
	std::string path;
	if (argc < 2)			// If path not set by the first parameter, use path to the current executable
	{
		path = argv[0];
		size_t i;
		for (i = path.size() - 1; i > 0; i--)
			if (path[i] == CST::Common::getSlashCharacter())
				break;
		path.erase(i, path.size() - i);
	}
	else
		path = argv[1];
	int port = 80;			// If port not set by second parameter, use port 80
	if (argc >= 3)
	{
		sscanf(argv[2], "%d", &port);
	}

	CST::HTTP::HTTPServer server(port);
	CST::HTTP::FileAccessProvider file_provider(server, path);
	CST::HTTP::CGIProvider cgi_provider(server, path);
	while (true)
		CST::Common::sleep(60000);
	return 1;
}
