/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* TCP client and server implementation over Berkley sockets                 */
/*****************************************************************************/
#ifndef __NETTCP_H__
#define __NETTCP_H__

#include "network/netcommon.h"
#include "common/databuffer.h"

#ifdef WIN32
#include <memory>
#else
#include <tr1/memory>
#endif

namespace CST
{
	namespace Network
	{

		const int64_t TCPServerID = -1;
		const int64_t TCPClientID = 0;

		enum TCPRetCode { TCPRC_SUCCESS, TCPRC_STOPPED, TCPRC_BADDATA };
		class NetEventHandler
		{
		protected:
			NetEventHandler() {};
		public:
			virtual ~NetEventHandler() {};
			virtual void onError(const int64_t& uid) = 0;				// Called on error
			virtual void onStop(const int64_t& uid) = 0;					// Called when data successfully sent
			virtual void onReceive(const int64_t& uid, CST::Common::blob* data) = 0;				// Called when new data arrived
			virtual void onSend(const int64_t& uid, const size_t remain) = 0;					// Called when data successfully sent
			virtual void onConnect(const int64_t& uid) = 0;				// Called when link is established
			virtual void onDisconnect(const int64_t& uid) = 0;			// Called when link is ended
		};

		class TCPServer;
		// TCP connection is basically INCOMING connection created by TCP server when incoming socket request comes
		// Each connection is executed in its own thread and works asynchronously
		// expected to be used internally in TCPServer.
		class TCPConnection
		{

		protected:
			bool running;
			int sock;
			int64_t uid;
			CST::Network::NetEventHandler* evh;
			CST::Network::TCPServer* tcps;

			CST::Common::thread *thr;
			CST::Common::mutex sync;

			CST::Common::mutex dataSync;
			CST::Common::DataBuffer mWriteBuffer;		// Data write buffer (send)

		public:
			TCPConnection(CST::Network::TCPServer* tcps, CST::Network::NetEventHandler* evh, const int clisock, const int64_t uid);
			virtual ~TCPConnection();
			void run();										// Main loop of connection processing
			void stop(void);

			// Sending just add new data chunk to the output buffer
			int send(CST::Common::blob& data);
			int64_t getUID(void) const;

		};

		// Server accepts incoming connections and create TCP connection per each one
		class TCPServer
		{
		protected:
			struct GARBAGE
			{
				int64_t id;
				TCPConnection* connection;
				unsigned long long time;
			};
		protected:
			bool running;
			int port;								// Incoming port
			int sock;
			int mMaxConnections;
			CST::Network::NetEventHandler* evh;

			CST::Common::thread *thr;						// Tread used for TCPServer object
			CST::Common::mutex sync;						// Thread safety mutex
			CST::Common::mutex protect_garbage, protect_connections;

			std::map<int64_t, TCPConnection*> connections;			// List of connection objects (for clean up)
			std::map<int64_t, GARBAGE> garbage;		// Removed connections to be deleted

			int64_t getConnUID(void);

		public:
			TCPServer(CST::Network::NetEventHandler* evh, const int port, const int maxconn = 30);
			void send(const int64_t& uid, CST::Common::blob& data);
			virtual ~TCPServer();
			void run();							// Main loop. Attention! Blocks current thread!
			void stop(void);
			void closeConnection(const int64_t& uid);
			void addGarbage(TCPConnection* connection);
			void clearGarbage();
		};

		// Client connection
		class TCPClient
		{

		protected:
			bool running;
			int port;								// Outgoing port
			std::string address;					// Target host
			int sock;
			CST::Network::NetEventHandler* evh;

			int64_t uid;

			CST::Common::thread *thr;				// Thread used for TCPClient object
			CST::Common::mutex sync;				// Thread safety mutex

			CST::Common::mutex dataSync;
			CST::Common::DataBuffer mWriteBuffer;		// Write buffer

		public:
			TCPClient(CST::Network::NetEventHandler* evh,
				const std::string address, const int port, const int64_t uid = TCPClientID);
			virtual ~TCPClient();
			void run();							// Main loop. Attention! Blocks the current thread!
			void stop(void);
			int send(CST::Common::DataBuffer& data);	// Send new data chunk

			int64_t get_uid(void) const { return uid; }
			CST::Network::NetEventHandler* get_evh(void) { return evh; }

			bool is_alive(void) const { return running; }
		};

	} // namespace Network
} // namespace CST

#endif

