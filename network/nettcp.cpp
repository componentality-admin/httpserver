/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* TCP client and server implementation over Berkley sockets                 */
/*****************************************************************************/

#include "common/common_memory.h"
#include "nettcp.h"
#include <iostream>
#include <algorithm>
const int TCP_BUF_SIZE = 20048;

#ifdef WIN32
#define MSG_DONTWAIT 0xFFFFFFFF
#define shutdown(sock, type)
#define REUSEADDR SO_REUSEADDR
#else
#define closesocket close
#define REUSEADDR SO_REUSEPORT
#endif

using namespace CST::Common;

bool readyToReceive(int sock, int interval = 100000);

// --------------------------------------------------------------------------------
class IdFinder
{
	int64_t uid;
public:
	IdFinder(int64_t uid) : uid(uid) {}

	bool operator() (CST::Network::TCPConnection* conn) const
	{
		return uid == conn->getUID();
	}
};

static void server_start(void* data)
{
	if (!data)
	{
		return;
	}

	CST::Network::TCPServer *s = (CST::Network::TCPServer*) data;
	s->run();
}

static void conn_start(void* data)
{
	if (!data)
	{
		return;
	}
	CST::Network::TCPConnection *c = (CST::Network::TCPConnection*) data;
	c->run();
}

static void client_start(void* data)
{
	if (!data)
	{
		return;
	}

	CST::Network::TCPClient *s = (CST::Network::TCPClient*) data;
	s->run();
}

// --------------------------------------------------------------------------------

CST::Network::TCPConnection::TCPConnection(CST::Network::TCPServer* tcps,
	CST::Network::NetEventHandler* evh, const int clisock, const int64_t uid)
	: sock(clisock)
	, running(true)
	, uid(uid)
	, evh(evh)
	, tcps(tcps)
{
	thr = CST_NEW(CST::Common::thread((CST::Common::thread::threadfunc) conn_start, this), "TCPConnection", "connection thread");
};

void CST::Network::TCPConnection::run()
{
	{
		isolate_sigpipe();	// Process Linux errors

		char buf[TCP_BUF_SIZE];

		struct timeval tv;
		tv.tv_sec = 3;
		tv.tv_usec = 0;

		evh->onConnect(uid);

		if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval)) < 0)
		{
			evh->onError(uid);
			evh->onDisconnect(uid);
			running = false;
			closesocket(sock);
			tcps->closeConnection(uid);
			std::cout << "Can't set SO_RCVTIMEO property: ATTENTION" << std::endl;
			thread::end();
			tcps->addGarbage(this);
			return;
		}

		while (running)
		{
			if (readyToReceive(sock))
				// Read data buffer
			{
#ifndef WIN32
				int toread = recv(sock, buf, TCP_BUF_SIZE, MSG_PEEK);
				if (toread <= 0)
				{
					CST::Common::sleep(50);
					switch (errno)
					{
					case EAGAIN:
						CST::Common::sleep(10);
						break;
					case EINTR:
						continue;
					default:
						std::cout << "Socket receive error: " << errno << std::endl;
						evh->onError(uid);
						evh->onDisconnect(uid);
						running = false;
						closesocket(sock);
						thread::end();
						tcps->addGarbage(this);
						return;
					}
				}
				int read = recv(sock, buf, toread, 0);
#else
				int read = recv(sock, buf, TCP_BUF_SIZE, 0);
				if (read == 0)
					CST::Common::sleep(50);
#endif
				if (read > 0)
				{
					CST::Common::blob dread(read);
					memcpy(dread.mData, buf, read);
					evh->onReceive(uid, &dread);
					dread.purge();
				}
				else
				{
#ifdef WIN32
					int err = WSAGetLastError();
					switch (err) {
					case WSAECONNRESET:
						evh->onError(uid);
						evh->onDisconnect(uid);
						running = false;
						closesocket(sock);
						thread::end();
						tcps->addGarbage(this);
						return;
					default:
						break;
					}
#endif
				}
			}
			else
			{
				CST::Common::sleep(50);
			}
			dataSync.lock();

			if (mWriteBuffer.size() != 0)
			{
				CST::Common::blob toprint(mWriteBuffer.size());
				mWriteBuffer.get((uint8_t*)toprint.mData, toprint.mSize);
				int sent = ::send(sock, toprint.mData, toprint.mSize, 0);
				toprint.purge();
				mWriteBuffer.clear();

				if (sent < 0)		// Socket is closed
				{
					std::cout << "Socket send error" << std::endl;
					evh->onError(uid);
					evh->onDisconnect(uid);
					running = false;
					closesocket(sock);
					dataSync.unlock();
					thread::end();
					tcps->addGarbage(this);
					return;
				}

				evh->onSend(uid, mWriteBuffer.size());
			}
			dataSync.unlock();

		}
		evh->onDisconnect(uid);
	}
	thread::end();
	tcps->addGarbage(this);
}

int CST::Network::TCPConnection::send(CST::Common::blob& data)
{
	if (!running) return TCPRC_STOPPED;
	if (data.mData == NULL) return TCPRC_BADDATA;
	if (data.mSize == 0) return TCPRC_BADDATA;

	CST::Common::scoped_lock l(dataSync);

	mWriteBuffer += data;

	return TCPRC_SUCCESS;
}

void CST::Network::TCPConnection::stop(void)
{
	// stop connections

	running = false;
	shutdown(sock, SHUT_RDWR);
	closesocket(sock);

	evh->onStop(uid);
}

CST::Network::TCPConnection::~TCPConnection()
{
	if (running) stop();

	mWriteBuffer.clear();

	CST_DELETE(thread, thr);
}

int64_t CST::Network::TCPConnection::getUID(void) const
{
	return uid;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
CST::Network::TCPServer::TCPServer(CST::Network::NetEventHandler* evh, const int port, const int maxconn)
	: port(port)
	, mMaxConnections(maxconn)
	, running(true)
	, evh(evh)
{
#ifdef WIN32
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}
#endif
thr = CST_NEW(CST::Common::thread((CST::Common::thread::threadfunc) server_start, this), "TCPServer", "server thread");
}

int64_t CST::Network::TCPServer::getConnUID(void)
{
	static int64_t i = CST::Network::TCPClientID + 1;
	++i;
	return i;
}

void CST::Network::TCPServer::send(const int64_t& uid, CST::Common::blob& data)
{
	scoped_lock lock(protect_connections);
	if (connections.find(uid) != connections.end())
		connections[uid]->send(data);
}

void CST::Network::TCPServer::run()
{
	int socket_type = SOCK_STREAM;
	int socket_proto = IPPROTO_TCP;
	sock = socket(PF_INET, socket_type, socket_proto);

	if (sock < 0)
	{
		evh->onError(TCPServerID);
		running = false;
		std::cout << "Can't create socket into server"<< std::endl;
		return;
	}

	struct sockaddr_in serv_addr, cli_addr;

	serv_addr.sin_family = PF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);

	int on = 1;
	if (setsockopt(sock, SOL_SOCKET, REUSEADDR, (char*)&on, sizeof(on)) < 0)
	{
		evh->onError(TCPServerID);
		running = false;
		closesocket(sock);
		std::cout << "Can't set REUSEADDR property"<< std::endl;
		return;
	}

	int result = bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
	if (result < 0)
	{
		evh->onError(TCPServerID);
		running = false;
		closesocket(sock);
		std::cout << "Can't assign the address to the socket"<< std::endl;
		return;
	}

	if (listen(sock, mMaxConnections) < 0)
	{
		evh->onError(TCPServerID);
		running = false;
		closesocket(sock);
		std::cout << "Can't accept incoming connection requests"<< std::endl;
		return;
	}

	while (running)
	{
		clearGarbage();
		socklen_t clilen = (socklen_t) sizeof(cli_addr);

		/* Accept actual connection from the client */
		int clisock = accept(sock, (struct sockaddr *)&cli_addr, &clilen);
		if (!running) return;

		if (clisock < 0)
		{
			evh->onError(TCPServerID);
			running = false;
			closesocket(sock);
			return;
		}

		{
			scoped_lock lock(protect_connections);
			int64_t uid = getConnUID();
			connections[uid] = CST_NEW(CST::Network::TCPConnection(this, evh, clisock, uid), "TCPServer", "tcp connection");
		}
	}
}

void CST::Network::TCPServer::closeConnection(const int64_t& uid)
{
	scoped_lock lock(protect_connections);
	if (connections.find(uid) != connections.end())
	{
		connections[uid]->stop();
		connections.erase(connections.find(uid));
	}
}

void CST::Network::TCPServer::stop(void)
{

	// stop server
	running = false;
	shutdown(sock, SHUT_RDWR);
	closesocket(sock);

	// stop connections
	for (std::map<int64_t, TCPConnection*>::iterator i = connections.begin(); i != connections.end(); i++)
	{
		i->second->stop();
		addGarbage(i->second);
	}

	evh->onStop(TCPServerID);
}

CST::Network::TCPServer::~TCPServer()
{
	if (running) stop();

	CST_DELETE(thread, thr);
}

void CST::Network::TCPServer::addGarbage(CST::Network::TCPConnection* connection)
{
	scoped_lock lock(protect_garbage);
	GARBAGE g;
	g.connection = connection;
	g.time = CST::Common::getSecondsCounter();
	g.id = connection->getUID();
	garbage[connection->getUID()] = g;
}

void CST::Network::TCPServer::clearGarbage()
{
	CST::Common::sleep(20);
	scoped_lock lock(protect_garbage);
	unsigned long long time = CST::Common::getSecondsCounter();
	for (std::map<int64_t, GARBAGE>::iterator i = garbage.begin(); i != garbage.end();)
	{
		if (time - i->second.time > 30)
		{
			CST_DELETE(TCPConnection, i->second.connection);
			std::map<int64_t, GARBAGE>::iterator to_delete = i;
			i++; 
			garbage.erase(to_delete);
		}
		else
			i++;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

CST::Network::TCPClient::TCPClient(CST::Network::NetEventHandler* evh,
	const std::string address, const int port, const int64_t uid)
	: address(address)
	, port(port)
	, running(true)
	, evh(evh)
	, uid(uid)
{
#ifdef WIN32
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}
#endif
	thr = CST_NEW(CST::Common::thread((CST::Common::thread::threadfunc) client_start, this), "TCPClient", "tcp thread");
}

CST::Network::TCPClient::~TCPClient()
{
	if (running) stop();

	mWriteBuffer.clear();

	CST_DELETE(thread, thr);
	CST_DELETE(EventHandler, evh);
}

bool readyToReceive(int sock, int interval)
{
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(sock, &fds);

	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = interval;

	int result = select(sock + 1, &fds, 0, 0, &tv);

	return result == 1;
}

void CST::Network::TCPClient::run()
{
	int socket_type = SOCK_STREAM;
	int socket_proto = IPPROTO_TCP;
	sock = socket(PF_INET, socket_type, socket_proto);

	if (sock < 0)
	{
		evh->onError(uid);
		running = false;
		std::cout << "Can't create socket into client"<< std::endl;
		return;
	}

	hostent* he = gethostbyname(address.c_str());

	struct sockaddr_in sa;
	sa.sin_family = PF_INET;
	sa.sin_addr.s_addr = *(u_long*)he->h_addr_list[0];
	sa.sin_port = htons(port);

	if (connect(sock, (struct sockaddr *)&sa, sizeof(sa)) < 0)
	{
		evh->onError(uid);
		running = false;
		closesocket(sock);
		std::cout << "Can't connect to socket with address " << address << " and port " << port<< std::endl;
		return;
	}

	isolate_sigpipe();

	char buf[TCP_BUF_SIZE];

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 300;

	evh->onConnect(uid);

	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval)) < 0)
	{
		evh->onError(uid);
		evh->onDisconnect(uid);
		running = false;
		closesocket(sock);
		std::cout << "Can't set SO_RCVTIMEO property"<< std::endl;
		return;
	}

	while (running)
	{
		if (readyToReceive(sock))
			// Read data buffer
		{
#ifndef WIN32
			int toread = recv(sock, buf, TCP_BUF_SIZE, MSG_PEEK);
			if (toread <= 0)
			{
				CST::Common::sleep(50);
				switch (errno)
				{
				case EAGAIN:
					CST::Common::sleep(10);
					break;
				case EINTR:
					continue;
				default:
					std::cout << "Socket receive error: " << errno << std::endl;
					evh->onError(uid);
					evh->onDisconnect(uid);
					running = false;
					closesocket(sock);
					return;
				}
			}
			int read = recv(sock, buf, toread, 0);
#else
			int read = recv(sock, buf, TCP_BUF_SIZE, 0);
			if (read == 0)
				CST::Common::sleep(50);
#endif
			if (read > 0)
			{
				CST::Common::blob dread(read);
				memcpy(dread.mData, buf, read);
				evh->onReceive(uid, &dread);
				dread.purge();
			}
			else
			{
#ifdef WIN32
				int err = WSAGetLastError();
				switch (err) {
				case WSAECONNRESET:
					evh->onError(uid);
					evh->onDisconnect(uid);
					running = false;
					closesocket(sock);
					return;
				default:
					break;
				}
#endif
			}
		}
		else
		{
			CST::Common::sleep(50);
		}
		dataSync.lock();

		if (mWriteBuffer.size() != 0)
		{
			CST::Common::blob toprint(mWriteBuffer.size());
			mWriteBuffer.get((uint8_t*) toprint.mData, toprint.mSize);
			int sent = ::send(sock, toprint.mData, toprint.mSize, 0);
			toprint.purge();
			mWriteBuffer.clear();

			if (sent < 0)		// Socket is closed
			{
				std::cout << "Socket send error"<< std::endl;
				evh->onError(uid);
				evh->onDisconnect(uid);
				running = false;
				closesocket(sock);
				dataSync.unlock();
				return;
			}

			evh->onSend(uid, mWriteBuffer.size());
		}
		dataSync.unlock();

	}
	evh->onDisconnect(uid);
}

void CST::Network::TCPClient::stop(void)
{
	// stop client

	std::cout << "Solicited socket stop"<< std::endl;

	running = false;
	shutdown(sock, SHUT_RDWR);
	closesocket(sock);

	evh->onStop(uid);
}

int CST::Network::TCPClient::send(CST::Common::DataBuffer& data)
{
	if (!running) return TCPRC_STOPPED;
	if (data.size() == 0) return TCPRC_BADDATA;

	CST::Common::scoped_lock l(dataSync);

	mWriteBuffer += data;

	return TCPRC_SUCCESS;
}

