/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* Network primitives (event handling)                                       */
/*****************************************************************************/

#ifndef __NETCOMMON_H__
#define __NETCOMMON_H__

#include <signal.h>
#include <stdio.h>
#include <errno.h>

#ifndef WIN32
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define closesocket close
#else
#include <Windows.h>
typedef int socklen_t;
#endif

#include "common/common_utilities.h"

#ifndef WIN32
inline void isolate_sigpipe(void)
{
	sigset_t sigpipe_mask;
	sigemptyset(&sigpipe_mask);
	sigaddset(&sigpipe_mask, SIGPIPE);
	sigset_t saved_mask;
	if (pthread_sigmask(SIG_BLOCK, &sigpipe_mask, &saved_mask) == -1)
		perror("pthread_sigmask");

}
#else
inline void isolate_sigpipe(void) {}
#endif


namespace CST
{
	namespace Network
	{

		// This class provides the basic abstraction for events handling. Connections are derived from event handler and lets
		// process main events happening with a socket link
		class EventHandler
		{
		public:
			EventHandler() {};
			virtual ~EventHandler() {};
		public:
			virtual void onReceive() {};				// Called when new data arrived
			virtual void onSend() {};					// Called when data successfully sent
			virtual void onConnect() {};				// Called when link is established
			virtual void onDisconnect() {};			// Called when link is ended
			
			virtual void onOverflow() {};				// Called when data buffer overflows (never for STL buffers)
			virtual void onUnderflow() {};				// Called when no more data (typically for sending). Not an error!
		};

		// Just a common abstraction for the entity which can do anything and proceed with events
		class IProcessor : public EventHandler
		{
		protected:
			EventHandler* mEventHandler;			// This class uses encapsulation and let user define own events processor
		public:
			IProcessor() : mEventHandler(NULL) {};
			IProcessor(EventHandler& evh) : mEventHandler(&evh) {};
			virtual ~IProcessor() {};
		public:
			virtual void run() = 0;					// The main process
		public:
			// These functions just re-address events to third party object
			virtual void onReceive() { if (mEventHandler) mEventHandler->onReceive(); };
			virtual void onSend() { if (mEventHandler) mEventHandler->onSend(); };
			virtual void onConnect() { if (mEventHandler) mEventHandler->onConnect(); };
			virtual void onDisconnect() { if (mEventHandler) mEventHandler->onDisconnect(); };
			virtual void onOverflow() { if (mEventHandler) mEventHandler->onOverflow(); };
			virtual void onUnderflow() { if (mEventHandler) mEventHandler->onUnderflow(); };
		};

		// Interface for derived classes managing how to utilize data blocks after being sent
		// Basically ISwappableRemover does nothing. But if block to be sent is dynamically allocated
		// then operator() must do delete to avoid memory junking.
	} // namespace Network
} // namespace CST

#endif

