/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2015                                        */
/*****************************************************************************/
/* Basic components and primitives                                           */
/*****************************************************************************/
#include "common_utilities.h"
#include <memory.h>
#include <stdlib.h>
#include <vector>

#ifdef _MSC_BUILD
#include <Windows.h>
#else
#include <pthread.h>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#endif

using namespace CST::Common;

/******************************************************************************************************/

#ifdef WIN32
thread::thread(thread::threadfunc func, void* arg, size_t stack_size)
{
	HANDLE thread = CreateThread(NULL, stack_size, (LPTHREAD_START_ROUTINE)func, arg, 0, NULL);
	mHandler = (void*)thread;
}

thread::~thread()
{
}

thread::operator void*()
{
	return mHandler;
}

void thread::join()
{
	WaitForSingleObject((HANDLE)mHandler, INFINITE);
}

void thread::end()
{
}

void CST::Common::sleep(const unsigned int msec)
{
	::Sleep(msec);
}

mutex::mutex()
{
	HANDLE* cs = new HANDLE(CreateMutexA(NULL, FALSE, NULL));
	mHandler = cs;
}

mutex::~mutex()
{
	HANDLE* cs = (HANDLE*)mHandler;
	ReleaseMutex(*cs);
	delete cs;
}

void mutex::lock()
{
	HANDLE* cs = (HANDLE*)mHandler;
	WaitForSingleObject(*cs, INFINITE);
}

void mutex::unlock()
{
	HANDLE* cs = (HANDLE*)mHandler;
	ReleaseMutex(*cs);
}

#else

thread::thread(thread::threadfunc func, void* arg, size_t stack_size)
{
	typedef void*(*pfunc)(void*);
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, stack_size);
	pthread_t* _thread = new pthread_t;
	pthread_create(_thread, &attr, (pfunc)func, arg);
	mHandler = _thread;
	pthread_attr_destroy(&attr);
}

thread::~thread()
{
	delete (pthread_t*) mHandler;
}

thread::operator void*()
{
	return mHandler;
}

void thread::join()
{
	pthread_join(*(pthread_t*)mHandler, NULL);
}

void thread::end()
{
	pthread_detach(pthread_self());
}

void CST::Common::sleep(const unsigned int msec)
{
	usleep(1000 * msec);
}

mutex::mutex()
{
	pthread_mutex_t* lock = new pthread_mutex_t;
	pthread_mutexattr_t   mta;
	pthread_mutexattr_init(&mta);
	pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(lock, &mta);
	mHandler = lock;
}

mutex::~mutex()
{
	pthread_mutex_t* lock = (pthread_mutex_t*)mHandler;
	pthread_mutex_unlock(lock);
	pthread_mutex_destroy(lock);
	delete lock;
}

void mutex::lock()
{
	pthread_mutex_t* lock = (pthread_mutex_t*)mHandler;
	pthread_mutex_lock(lock);
}

void mutex::unlock()
{
	pthread_mutex_t* lock = (pthread_mutex_t*)mHandler;
	pthread_mutex_unlock(lock);
}

#endif

unsigned long long CST::Common::getSecondsCounter()
{
	return getTimeMilliSec() / 1000;
}

/***************************************************************************************/
#include <fstream>
#ifdef WIN32
#include <Windows.h>
#else
#include <dirent.h>
#endif

#ifndef WIN32
#define SLASH '/'
#else
#define SLASH '\\'
#endif

// Concatenate two blobs. Produces dynamic memory object
blob CST::Common::operator+(const blob a, const blob b)
{
	blob result(a.mSize + b.mSize);
	if (a.mSize)
		memcpy(result.mData, a.mData, a.mSize);
	if (b.mSize)
		memcpy(result.mData + a.mSize, b.mData, b.mSize);
	return result;
}

// Split blob at given location
std::pair<blob, blob> CST::Common::blob_split(const blob a, const size_t index)
{
	blob _a(index);
	blob _b(a.mSize - index);
	if (_a.mSize)
		memcpy(_a.mData, a.mData, _a.mSize);
	if (_b.mSize)
		memcpy(_b.mData, a.mData + _a.mSize, _b.mSize);
	return std::pair<blob, blob>(_a, _b);
}

// Read entire file to a [binary] string
blob CST::Common::fileRead(const std::string& name)
{
	File ifile;
	blob result;
	ifile.open(name, File::READ);
	if (ifile.bad() || !ifile.good())
	{
		ifile.close();
		return result;
	}
	result.mSize = ifile.size();
	if (result.mSize > 10000000)
	{
		ifile.close();
		return blob();
	}
	if (result.mSize > 0)
	{
		result.mData = (char*) CST_ALLOC(result.mSize, "file read", "read data");
		ifile.read(result.mData, result.mSize);
	}
	ifile.close();
	return result;
}

// Write entire file from a binary string
bool CST::Common::fileWrite(const std::string& name, const blob& content)
{

	File ofile;
	ofile.open(name, File::WRITE);
	ofile.write(content.mData, content.mSize);
	ofile.close();
	return true;
}

// Check if file exists
size_t CST::Common::fileSize(const std::string name)
{
	File ifile;
	ifile.open(name, File::READ);
	bool result = ifile.good() && !ifile.bad();
	size_t size = ifile.size();
	ifile.close();
	if (result)
		return size;
	else
		return 0;
}

// Check if file exists
bool CST::Common::fileExists(const std::string name)
{
	File ifile;
	ifile.open(name, File::READ);
	bool result = ifile.good() && !ifile.bad();
	ifile.close();
	return result;
}

// Copy file
bool CST::Common::fileCopy(const std::string& dst, const std::string& src, const size_t chunk_size)
{
	File ifile;
	File ofile;
	ifile.open(src, File::READ);
	bool result = ifile.good() && !ifile.bad();
	if (!result)
	{
		ifile.close();
		return result;
	}
	ofile.open(dst, File::WRITE);
	result = ofile.good() && !ofile.bad();
	if (!result)
	{
		ofile.close();
		return result;
	}
	size_t size = ifile.size();
	char* buffer = (char*) CST_ALLOC(chunk_size, "file copy", "copy buffer");
	for (size_t i = 0; i < size;)
	{
		size_t readsize = (size - i > chunk_size) ? chunk_size : (size - i);
		ifile.read(buffer, readsize);
		ofile.write(buffer, readsize);
		i += readsize;
	};
	CST_FREE(buffer);
	ifile.close();
	ofile.close();
	return true;
};

// Concatenate directory and file paths
std::string CST::Common::fileJoinPaths(const std::string& dir, const std::string& file)
{
	if (dir.size() == 0)
		return file;
	if (file.size() == 0)
		return dir;
	if (dir[dir.size() - 1] == SLASH)
		return dir + file;
	return dir + SLASH + file;
}

std::string CST::Common::fileShortName(const std::string& fname)
{
	std::string shorted_name = fname;
	char slash = getSlashCharacter();
	for (int i = (int)fname.size() - 1; i >= 0; i--)
	{
		if (fname[i] == slash)
		{
			shorted_name = fname.substr((size_t)i + 1);
			break;
		}
	}
	return shorted_name;
}

// List all files in the given directory
std::list<std::string> CST::Common::listFiles(const std::string& dir)
{
#ifdef WIN32
	{
		std::list<std::string> names;
		std::string search_path = fileJoinPaths(dir, "*.*");
		WIN32_FIND_DATAA fd;
		HANDLE hFind = ::FindFirstFileA(search_path.c_str(), &fd);
		if (hFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					names.push_back(fd.cFileName);
				}
			} while (::FindNextFileA(hFind, &fd));
			::FindClose(hFind);
		}
		return names;
	}
#else
	{
	std::list<std::string> names;
	DIR *_dir;
	struct dirent *de;

	_dir = opendir(dir.c_str());
	while (_dir)
	{
		de = readdir(_dir);
		if (!de) break;
		switch (de->d_type) {
			case DT_REG:
				names.push_back(de->d_name);
				break;

			case DT_UNKNOWN:
				struct stat ss;
				std::string inode_name = dir + "/" + de->d_name;
				int status;

				do {
					if ((status = stat(inode_name.c_str(), &ss)) < 0 && errno == EINTR)
						continue;
					break;
				} while (1);

				if (!status && S_ISREG(ss.st_mode))
					names.push_back(de->d_name);

				break;
		}
	}
	closedir(_dir);
	return names;
}
#endif
}

// List all links in the given directory
std::list<std::string> CST::Common::listLinks(const std::string& dir)
{
	std::list<std::string> names;
#ifdef WIN32
	return names;
#else
	DIR *_dir;
	struct dirent *de;

	_dir = opendir(dir.c_str());
	while (_dir)
	{
		de = readdir(_dir);
		if (!de) break;
		if (de->d_type == DT_LNK) {
			names.push_back(de->d_name);
		}
	}
	closedir(_dir);
	return names;
#endif
}

// List all subfolders in the given directory
std::list<std::string> CST::Common::listSubdirectories(const std::string& dir)
{
#ifdef WIN32
	{
		std::list<std::string> names;
		std::string search_path = fileJoinPaths(dir, "*.*");
		WIN32_FIND_DATAA fd;
		HANDLE hFind = ::FindFirstFileA(search_path.c_str(), &fd);
		if (hFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					std::string toadd = fd.cFileName;
					if ((toadd != ".") && (toadd != ".."))
						names.push_back(toadd);
				}
			} while (::FindNextFileA(hFind, &fd));
			::FindClose(hFind);
		}
		return names;
	}
#else
	{
	std::list<std::string> names;
	DIR *_dir;
	struct dirent *de;

	_dir = opendir(dir.c_str());
	while (_dir)
	{
		de = readdir(_dir);
		if (!de) break;
		if (de->d_type == DT_DIR)
		{
			std::string toadd = de->d_name;
			if ((toadd != ".") && (toadd != ".."))
				names.push_back(toadd);
		}
	}
	closedir(_dir);
	return names;
}
#endif
}


// Encode all pre-space characters
blob CST::Common::escapeCoding(const blob src)
{
	std::string result;
	for (size_t i = 0; i < src.mSize; i++)
	{
		if ((src.mData[i] == 1) || (src.mData[i] == 0))
		{
			char c = src.mData[i];
			result += 1;
			c |= 0xF0;
			result += c;
		}
		else
			result += src.mData[i];
	}
	blob _result;
	_result.mSize = result.size();
	_result.mData = (char*) CST_ALLOC(result.size(), "escape coding", "result");
	memcpy(_result.mData, result.c_str(), result.size());
	return _result;
}

// Decode string with pre-space characters encoded
blob CST::Common::escapeDecoding(const blob src)
{
	std::string result;
	for (size_t i = 0; i < src.mSize; i++)
	{
		if ((src.mData[i] == 1) && (i < src.mSize - 1))
		{
			char c = src.mData[i + 1];
			c &= ~(char)0xF0;
			result += c;
			i += 1;
		}
		else
			result += src.mData[i];
	}
	blob _result;
	_result.mSize = result.size();
	_result.mData = (char*) CST_ALLOC(result.size(), "escape decoding", "result");
	memcpy(_result.mData, result.c_str(), result.size());
	return _result;
}

char CST::Common::getSlashCharacter()
{
	return SLASH;
}

/***********************************************************************************************/

File::File()
{
}

File::~File()
{
	if (mHandler.is_open())
		close();
	filelist[mName] = NULL;
}

void File::open(const std::string name, const File::READWRITE rw)
{
	mName = name;
	mLock.lock();
	while (filelist[mName] != NULL)
	{
		mLock.unlock();
		CST::Common::sleep(500);
		mLock.lock();
	}
	filelist[mName] = &mHandler;
	if (rw == READ)
		mHandler.open(name.c_str(), std::ios::binary | std::ios::in);
	else
		mHandler.open(name.c_str(), std::ios::binary | std::ios::out);
	if (!mHandler.is_open())
	{
		filelist[mName] = NULL;
	}
	mLock.unlock();
}

bool File::good()
{
	return mHandler.is_open() && mHandler.good();
}

bool File::bad()
{
	return !mHandler.is_open() || mHandler.bad();
}

bool File::eof()
{
	return !mHandler.is_open() || mHandler.eof();
}

size_t File::read(const char* buf, size_t size)
{
	if (!mHandler.is_open())
		return 0;
	mHandler.read((char*)buf, size);
	return (size_t)mHandler.gcount();
}

void File::write(const char* buf, size_t size)
{
	if (!mHandler.is_open())
		return;
	mHandler.write((char*)buf, size);
	return;
}

blob File::read()
{
	size_t _size = size();
	blob result(_size);
	result.mSize = read(result.mData, result.mSize);
	return result;
}

void File::write(blob data)
{
	write(data.mData, data.mSize);
}

void File::close()
{
	mLock.lock();
	if (mHandler.is_open())
		mHandler.close();
	filelist[mName] = NULL;
	mLock.unlock();
}

size_t File::size()
{
	if (!mHandler.is_open())
		return 0;
	mHandler.seekg(0, std::ios::end);
	size_t result = (size_t)mHandler.tellg();
	mHandler.seekg(0);
	return result;
}

unsigned long long File::lsize()
{
	if (!mHandler.is_open())
		return 0;
	mHandler.seekg(0, std::ios::end);
	unsigned long long result = (unsigned long long)mHandler.tellg();
	mHandler.seekg(0);
	return result;
}

std::map<std::string, std::fstream*> File::filelist;

/*********************************************************************************/

ThreadSet::ThreadSet(const bool autodelete)
{ 
	mExit = false; 
	mAutoDelete = autodelete; 
}

ThreadSet::~ThreadSet()
{
	exit();
	for (std::map<thread::threadfunc, thread*>::iterator i = mThreads.begin(); i != mThreads.end(); i++)
	{
		i->second->join();
		CST_DELETE(thread, i->second);
	}
}

void ThreadSet::run(thread::threadfunc threadf)
{
	run(threadf, NULL);
}

void ThreadSet::run(thread::threadfunc threadf, void* data)
{
	mLock.lock();
	if (mThreads.find(threadf) != mThreads.end())
		if (!mAutoDelete)
			throw "Thread is already running";
		else
		{
			CST_DELETE(thread, mThreads[threadf]);
		}
	mThreads[threadf] = CST_NEW(thread(threadf, data), "ThreadSet", "new thread");
	mLock.unlock();
}

void ThreadSet::exit()
{
	mLock.lock();
	mExit = true;
	mLock.unlock();
}

bool ThreadSet::getExit()
{
	bool result = mExit;
	return result;
}

std::string CST::Common::blobToString(const blob b)
{
	std::string result;
	result.assign(b.mData, b.mSize);
	return result;
}

blob CST::Common::stringToBlob(const std::string& s)
{
	blob result;
	result.mSize = s.size();
	result.mData = (char*)s.c_str();
	return result;
}

#ifndef _MSC_BUILD
static void mkdirectory(std::string name)
{
	mkdir(name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}
static void rmdirectory(std::string name)
{
	unlink(name.c_str());
}
std::string CST::Common::resolveSymlink(std::string link)
{
	char buf[256] = {0};
	readlink(link.c_str(), buf, 256);
	return buf;
}
void CST::Common::runProcess(std::string cmd)
{
	system(cmd.c_str());
}
#else
static void mkdirectory(std::string name)
{
	CreateDirectoryA(name.c_str(), NULL);
}
static void rmdirectory(std::string name)
{
	RemoveDirectoryA(name.c_str());
}
std::string CST::Common::resolveSymlink(std::string link)
{
	HANDLE h = CreateFileA(link.c_str(), GENERIC_READ, FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);
	char buf[256];
	GetFinalPathNameByHandleA(h, buf, 256, FILE_NAME_NORMALIZED);
	return buf;
}
void CST::Common::runProcess(std::string cmd)
{
	STARTUPINFOA info = { sizeof(info) };
	PROCESS_INFORMATION processInfo;
	std::string cmdline = std::string("cmd.exe /C ") + cmd;
	if (CreateProcessA(NULL, (char*) (cmdline).c_str(), NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo))
	{
		::WaitForSingleObject(processInfo.hProcess, INFINITE);
		CloseHandle(processInfo.hProcess);
		CloseHandle(processInfo.hThread);
	}
}
#endif

void CST::Common::makeDirectory(const std::string& name)
{
	std::string _name = name;
	mkdirectory(_name);
}

void CST::Common::removeDirectory(const std::string& name)
{
	std::string _name = name;
	rmdirectory(_name);
}

static const char HEXLINE[] = "0123456789ABCDEF";

std::string CST::Common::hexEncode(const std::string src)
{
	std::string result;
	for (size_t i = 0; i < src.length(); i++)
	{
        unsigned char c0 = (unsigned char) ( (src[i] >> 0) & 0x0F );
        unsigned char c1 = (unsigned char) ( (src[i] >> 4) & 0x0F );
        result += HEXLINE[(int)c1];
        result += HEXLINE[(int)c0];
	}
	return result;
}

static inline char digitToNum(char c)
{
	if ((c >= '0') && (c <= '9'))
		return c - '0';
	if ((c >= 'A') && (c <= 'F'))
		return c - 'A' + 10;
	return 0;
}

std::string CST::Common::hexDecode(const std::string src)
{
	std::string result;
	for (size_t i = 0; i < src.length() / 2; i ++)
	{
		char c1 = src[i * 2];
		char c0 = src[i * 2 + 1];
		c1 = digitToNum(c1);
		c0 = digitToNum(c0);
		char c = (c1 << 4) | c0;
		result += c;
	}
	return result;
}


//////////////////////////////////////////////////////////////////
//   Auxillary functions
//////////////////////////////////////////////////////////////////
std::vector<std::string>& CST::Common::split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim))
	{
		elems.push_back(item);
	}
	return elems;
}

std::string CST::Common::trim(const std::string src)
{
	size_t start_idx;
	std::string result;
	for (start_idx = 0; start_idx < src.size(); start_idx++)
	{
		if (src[start_idx] > ' ')
			break;
	}
	result = src.substr(start_idx);
	for (int end_idx = (int)result.size() - 1; end_idx >= 0;)
	{
		if (result[end_idx] < ' ')
			result = result.substr(0, end_idx);
		else
			break;
		end_idx--;
	}
	return result;
}

unsigned long long CST::Common::getTimeMilliSec()
{
#ifndef WIN32
	struct timeval tv;
	unsigned long long msec;
	gettimeofday(&tv, NULL);
	msec = tv.tv_sec * 1000ULL + tv.tv_usec / 1000ULL;
	return msec;
#else
	return GetTickCount64();
#endif
}

std::string CST::Common::upcase(const std::string& source)
{
	std::string result;
	for (size_t i = 0; i < source.size(); i++)
		if ((source[i] >= 'a') && (source[i] <= 'z'))
			result.push_back(source[i] - 'a' + 'A');
		else
			result.push_back(source[i]);
	return result;
}

std::string CST::Common::lowcase(const std::string& source)
{
	std::string result;
	for (size_t i = 0; i < source.size(); i++)
		if ((source[i] >= 'A') && (source[i] <= 'Z'))
			result.push_back(source[i] + 'a' - 'A');
		else
			result.push_back(source[i]);
	return result;
}
