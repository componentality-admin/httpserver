/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* Data busser abstraction (alternate to CST::Common::blob and extending it) */
/*****************************************************************************/
#ifndef DATABUFFER_H
#define DATABUFFER_H

#include <vector>
#include <stdint.h>           // uint*_t
#include "common_utilities.h" // CST::Common::blob

namespace CST
{
	namespace Common
	{
		class DataBuffer
		{
			std::vector<uint8_t> data;
		public:
			// STL compatibility
			typedef std::vector<uint8_t>::const_iterator const_iterator;
			typedef std::vector<uint8_t>::iterator iterator;
			typedef std::vector<uint8_t>::const_reference const_reference;
			typedef std::vector<uint8_t>::reference reference;
			typedef std::vector<uint8_t>::size_type size_type;
			typedef std::vector<uint8_t>::value_type value_type;

			iterator begin() { return data.begin(); };
			const_iterator begin() const { return data.begin(); };
			iterator end() { return data.end(); };
			const_iterator end() const { return data.end(); };
			
			reference operator[] (size_type n) { return data[n]; };
			const_reference operator[] (size_type n) const { return data[n]; };

			size_type size() const { return data.size(); };

			iterator erase (iterator position) { return data.erase(position); };

			iterator erase (iterator first, iterator last) { return data.erase(first, last); };

			void resize (size_type n, value_type val = value_type()) {data.resize(n, val); };
			// STL compatibility
			
			DataBuffer();
			DataBuffer(const DataBuffer& rhs);
			DataBuffer(const uint8_t* d, const size_t s);
			DataBuffer(const CST::Common::blob& rhs);

			size_t get(uint8_t* d, const size_t s);

			bool operator==(const DataBuffer& rhs) const;
			bool operator!=(const DataBuffer& rhs) const;

			DataBuffer& operator=(const DataBuffer& rhs);
			DataBuffer& operator+(const DataBuffer& rhs);
			DataBuffer& operator+=(const DataBuffer& rhs);
			DataBuffer& operator=(const CST::Common::blob& rhs);
			DataBuffer& operator+(const CST::Common::blob& rhs);
			DataBuffer& operator+=(const CST::Common::blob& rhs);

			void clear(void);
		};

	}
}

#endif // !defined DATABUFFER_H
