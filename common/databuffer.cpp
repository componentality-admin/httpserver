/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* Data busser abstraction (alternate to CST::Common::blob and extending it) */
/*****************************************************************************/
#include "databuffer.h"
#include <string.h> // memset
#include <iterator>

using namespace CST::Common;

DataBuffer::DataBuffer()
{
}

DataBuffer::DataBuffer(const DataBuffer& rhs)
	: data(rhs.data)
{
}

DataBuffer::DataBuffer(const uint8_t* d, const size_t s)
{
	data.reserve(s);
	std::copy(d, d+s, std::back_inserter(data));
}

DataBuffer::DataBuffer(const CST::Common::blob& rhs)
{
	if(rhs.mSize > 0)
	{
		data.reserve(rhs.mSize);
		std::copy(rhs.mData, rhs.mData+rhs.mSize, std::back_inserter(data));
	}
}


size_t DataBuffer::get(uint8_t* d, const size_t s)
{
	if(size() == 0) return 0;

	size_t size_to_copy = (s<size())?s:size();
	memset(d, 0, s);
	std::copy(begin(), begin()+size_to_copy, d);
	data.erase(begin(), begin()+size_to_copy);
	return size_to_copy;
}

bool DataBuffer::operator==(const DataBuffer& rhs) const
{
	return ((size() == rhs.size()) && (data == rhs.data));
}

bool DataBuffer::operator!=(const DataBuffer& rhs) const
{
	return !(*this == rhs);
}

DataBuffer& DataBuffer::operator=(const DataBuffer& rhs)
{
	if(rhs != *this)
	{
		data = rhs.data;
	}

	return *this;
}

DataBuffer& DataBuffer::operator=(const CST::Common::blob& rhs)
{
	DataBuffer tmp(rhs);
	data = tmp.data;

	return *this;
}

DataBuffer& DataBuffer::operator+(const DataBuffer& rhs)
{
	if(rhs.size() > 0)
	{
		data.reserve(data.size() + rhs.data.size());
		std::copy(rhs.begin(), rhs.end(), std::back_inserter(data));
	}

	return *this;
}

DataBuffer& DataBuffer::operator+=(const DataBuffer& rhs)
{
	*this = *this + rhs;
	return *this;
}

DataBuffer& DataBuffer::operator+(const CST::Common::blob& rhs)
{
	if(rhs.mSize > 0 && rhs.mData)
	{
		data.reserve(data.size() + rhs.mSize);
		std::copy(rhs.mData, rhs.mData+rhs.mSize, std::back_inserter(data));
	}

	return *this;
}

DataBuffer& DataBuffer::operator+=(const CST::Common::blob& rhs)
{
	*this = *this + rhs;
	return *this;
}

void DataBuffer::clear(void)
{
	data.clear();
}

