/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* Controllable memory allocation and leaks prevention                       */
/*****************************************************************************/
#ifndef __COMMON_MEMORY_H__
#define __COMMON_MEMORY_H__

#include <list>
#include <map>
#include <string>

#define CST_ALLOC(size, owner, goal) \
  (CST::Common::Memory::getInstance().allocate(size, owner, goal, __FILE__, __FUNCTION__, __LINE__))
#define CST_REALLOC(ptr, size, owner, goal) \
  (CST::Common::Memory::getInstance().reallocate(ptr, size, owner, goal, __FILE__, __FUNCTION__, __LINE__))
#define CST_FREE(ptr) \
	{if (ptr) CST::Common::Memory::getInstance().deallocate(ptr);}
#define CST_NEW(classname, owner, goal) (new(CST_ALLOC(sizeof(classname), owner, goal)) classname)
#define CST_NEW_ARR(classname, dim, owner, goal) (new(CST_ALLOC(sizeof(classname), owner, goal)) classname[dim])
#define CST_DELETE(classname, ptr) {if (ptr) {((classname*)(ptr))->~classname(); CST_FREE((classname*) ptr);}}
#define CST_DELETE_ARR(classname, ptr) \
{if (ptr) {size_t dim = CST::Common::Memory::getInstance().get_dimension(ptr); for (size_t i = 0; i < dim; i++) (((classname*)(ptr))+i)->~classname(); CST_FREE((classname*) ptr);}}

#ifdef _DEBUG
#define CST_HEAP_FILL true
#define CST_HEAP_START_GUARD 16
#define CST_HEAP_END_GUARD 16
#else
#define CST_HEAP_FILL false
#define CST_HEAP_START_GUARD 0
#define CST_HEAP_END_GUARD 0
#endif

namespace CST
{
	namespace Common
	{
		class Memory
		{
		protected:
			struct METADATA
			{
				void* mOriginalPtr;		// Pointer to the user's block
				size_t mSize;			// Requested size
				std::string mGoal;		// Goal of block's allocation
				std::string mOwner;		// Block owner
				std::string mFile;		// File where block allocated
				size_t mLine;			// Code line where block allocated
				std::string mFunction;	// Function where block allocated
				size_t mArrayDimension; // Array dimension
			};
		protected:
			std::map<void*, METADATA> mMetadata;
			unsigned long long mTotalSize;
			static Memory* mInstance;
			bool mDiagStarted;
			std::map<void*, METADATA> mDiagnostic;
		protected:
			Memory() { mTotalSize = 0; };
			virtual ~Memory() {};
			virtual void* alloc(const size_t);
			virtual void dealloc(void*);
		public:
			static Memory& getInstance();
			void* allocate(const size_t size, const std::string owner, const std::string goal, const std::string file, const std::string function, const size_t line);
			void* reallocate(void*, const size_t size, const std::string owner, const std::string goal, const std::string file, const std::string function, const size_t line);
			void deallocate(void*);
			void* allocate_array(const size_t size, const size_t dimension, const std::string owner, const std::string goal, const std::string file, const std::string function, const size_t line);
			void deallocate_array(void*);
			size_t get_dimension(void*);
		public:
			void report(const std::string file, const bool csv = false);
			void start_diagnostic();
			void stop_diagnostic();
			void report_diagnostic(const std::string file, const bool csv = false);
		protected:
			static void report(std::ofstream&, std::map<void*, METADATA>, const unsigned long long);
			static void reportCSV(std::ofstream&, std::map<void*, METADATA>, const unsigned long long);
		};

		class IHeap
		{
		public:
			virtual void* alloc(const size_t size) = 0;
			virtual void  free(void*) = 0;
		};

		class EasyHeap : public IHeap
		{
		protected:
			void* mLock;
			size_t mTotalSize;
			size_t mCurrentSize;
			size_t mStartGuard;
			size_t mEndGuard;
			bool   mPatternFill;
			char*  mMemory;
		public:
			EasyHeap(const size_t size, const size_t start_guard_size = CST_HEAP_START_GUARD, 
				const size_t end_guard_size = CST_HEAP_END_GUARD, const bool fill = CST_HEAP_FILL);
			virtual ~EasyHeap();
		public:
			virtual void* alloc(const size_t size);
			virtual void  free(void*);
		};

		class StdHeap : public IHeap
		{
		public:
			StdHeap(const size_t size) {};
			virtual ~StdHeap() {};
		public:
			virtual void* alloc(const size_t size) { return new char[size]; };
			virtual void  free(void* ptr) { delete[](char*) ptr; };
		};

		class SlottedHeap : public IHeap
		{
		protected:
			size_t mSlotSize;
			size_t mTotalSize;
			size_t mCurrentSize;
			bool* mAllocationTable;
			char*  mMemory;
			void* mLock;
		public:
			SlottedHeap(const size_t slot_size, const size_t slot_amount);
			virtual ~SlottedHeap();
		public:
			virtual void* alloc(const size_t size);
			virtual void  free(void* ptr);
		};

		class ChainHeap : public IHeap
		{
		protected:
			std::list<IHeap*> mHeaps;
			std::map<void*, IHeap*> mPtrMaps;
			void* mLock;
		public:
			ChainHeap();
			virtual ~ChainHeap();
			virtual ChainHeap& add(IHeap& heap) { mHeaps.push_back(&heap); return *this; };
		public:
			virtual void* alloc(const size_t size);
			virtual void  free(void* ptr);
		};

		class SizeSelectiveHeap : public IHeap
		{
		protected:
			IHeap& mHeap;
			const size_t mThreshold;
		public:
			SizeSelectiveHeap(IHeap& heap, const size_t threshold) : mHeap(heap), mThreshold(threshold) {};
			virtual ~SizeSelectiveHeap() {};
		public:
			virtual void* alloc(const size_t size) { if (size <= mThreshold) return mHeap.alloc(size); else return NULL; };
			virtual void  free(void* ptr) { mHeap.free(ptr); };
		};

	}
}

#endif