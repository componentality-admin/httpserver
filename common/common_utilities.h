/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2015                                        */
/*****************************************************************************/
/* Basic components and primitives                                           */
/*****************************************************************************/

#ifndef COMMON_UTILITIES_H
#define COMMON_UTILITIES_H

#include "common_memory.h"
#include <string>
#include <map>
#include <fstream>
#include <list>
#include <vector>
#include <sstream>
#include <cstring>

#ifndef NULL
#define NULL (0L)
#endif

#ifndef ____UNDEFINED
#define ____UNDEFINED (size_t)-1
#endif


namespace CST
{
	namespace Common
	{

		class thread
		{
		public:
			typedef void(*threadfunc)(...);
		protected:
			void* mHandler;
		public:
			thread(threadfunc, void* = NULL, size_t stack_size = 200000);
			virtual ~thread();
			virtual operator void*();
			virtual void join();
			static  void end();
		};

		class mutex
		{
		protected:
			void* mHandler;
		public:
			mutex();
			virtual ~mutex();
			virtual void lock();
			virtual void unlock();
		};

		class scoped_lock
		{
		private:
			mutex& val;
		public:
			scoped_lock(mutex& val)
				: val(val)
			{
				val.lock();
			}

			~scoped_lock()
			{
				val.unlock();
			}
		};

		void sleep(const unsigned int msec);
		unsigned long long getSecondsCounter();

		struct blob
		{
			char* mData;
			size_t mSize;
			blob() { mData = NULL; mSize = 0; }
			blob(const size_t size) { mSize = size; if (mSize) mData = (char*) CST_ALLOC(mSize, "blob", "default"); else mData = NULL; }
			blob(const blob& src) { mData = src.mData; mSize = src.mSize; }
			virtual ~blob() { }
			blob& operator=(const blob& src) { mData = src.mData; mSize = src.mSize; return *this; }
			void purge() { if (mData) CST_FREE(mData); mData = NULL; mSize = 0; }
		};

		blob operator+(const blob a, const blob b);
		std::pair<blob, blob> blob_split(const blob a, const size_t index);

		// Read entire file content
		blob fileRead(const std::string& name);
		// Write entire file content
		bool fileWrite(const std::string& name, const blob& content);
		// Check file existence
		bool fileExists(const std::string name);
		// Get file size
		size_t fileSize(const std::string name);
		// Combine directory and file path
		std::string fileJoinPaths(const std::string& dir, const std::string& file);
		// List all files in the folder
		std::list<std::string> listFiles(const std::string& dir);
		// List all symlinks in the folder
		std::list<std::string> listLinks(const std::string& dir);
		// List all subfolders in the folder
		std::list<std::string> listSubdirectories(const std::string& dir);
		// Get file name without path
		std::string fileShortName(const std::string& fname);
		// Copy file
		bool fileCopy(const std::string& dst, const std::string& src, const size_t chunk_size = 1000000);
		// Make directory
		void makeDirectory(const std::string& name);
		// Remove directory
		void removeDirectory(const std::string& name);
		// Do escape coding and decoding of the given data set
		blob escapeCoding(const blob src);
		blob escapeDecoding(const blob src);
		// Get current OS slash character
		char getSlashCharacter();
		// Remove leading and ending spaces
		std::string trim(const std::string src);
		// split string by delim
		std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
		// Convert BLOB to string (no memory allocation)
		std::string blobToString(const blob);
		// Convert string to BLOB (no memory allocation)
		blob stringToBlob(const std::string&);
		// Hex conversions
		std::string hexEncode(const std::string src);
		std::string hexDecode(const std::string src);
		// Resolve symbolic link
		std::string resolveSymlink(std::string);
		// Run command
		void runProcess(std::string);

		class File
		{
		public:
			enum READWRITE
			{
				READ,
				WRITE
			};
		protected:
			static std::map<std::string, std::fstream*> filelist;
		protected:
			std::string mName;
			std::fstream mHandler;
			mutex mLock;
		public:
			File();
			virtual ~File();
			virtual void open(const std::string name, const READWRITE rw);
			virtual bool good();
			virtual bool bad();
			virtual bool eof();
			virtual size_t read(const char*, size_t size);
			virtual blob read();
			virtual void write(const char*, size_t size);
			virtual void write(blob);
			virtual void close();
			virtual size_t size();
			virtual unsigned long long lsize();
		};

		class ThreadSet
		{
		protected:
			mutex mLock;
			std::map<thread::threadfunc, thread*> mThreads;
			bool mExit;
			bool mAutoDelete;
		public:
			ThreadSet(const bool autodelete = false);
			virtual ~ThreadSet();
			virtual void run(thread::threadfunc threadf);
			virtual void run(thread::threadfunc threadf, void*);
			virtual void exit();
			virtual bool getExit();
		};

		template <typename T>
		std::string ntoa(T n)
		{
			std::ostringstream ss;
			ss << n;
			return ss.str();
		}

		template <typename N>
		blob numberToBlob(N value)
		{
			blob result(sizeof(N));
			for (size_t i = 0; i < sizeof(N); i++)
			{
				result.mData[i] = value & 0xFF;
				value >>= 8;
			}
			return result;
		}

		template <typename N>
		N blobToNumber(blob b)
		{
			N result = 0;
			for (size_t i = 0; i < sizeof(N); i++)
			{
				N temp = 0;
				temp |= (unsigned char)b.mData[i];
				temp <<= i * 8;
				result |= temp;
			}
			return result;
		}

		unsigned long long getTimeMilliSec();

		// Convert string to upper/lower case
		std::string upcase(const std::string&);
		std::string lowcase(const std::string&);

	}
}

#endif

