/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2017                                        */
/*****************************************************************************/
/* Controllable memory allocation and leaks prevention                       */
/*****************************************************************************/
#include "common_memory.h"
#include "common_utilities.h"

using namespace CST::Common;

#ifndef ____min
#define ____min(x, y) ((x) < (y) ? (x) : (y))
#endif

#ifndef HEAP_GUARD_AMOUNT
#define HEAP_GUARD_AMOUNT 0
#endif

Memory* Memory::mInstance = NULL;
mutex* MetadataLock = NULL;

Memory& Memory::getInstance()
{
	if (!mInstance)
		mInstance = new Memory();
	return *mInstance;
}

void* Memory::alloc(const size_t size)
{
	return new (std::nothrow) char[size];
}

void Memory::dealloc(void* ptr)
{
	delete[](char*)ptr;
}

void* Memory::reallocate(void* ptr, const size_t size, const std::string owner, const std::string goal, const std::string file, const std::string function, const size_t line)
{
	scoped_lock lock(*MetadataLock);
	void* new_ptr = allocate(size, owner, goal, file, function, line);
	if (mMetadata.find(ptr) != mMetadata.end())
	{
		size_t dim = mMetadata[ptr].mArrayDimension;
		size_t oldsize = mMetadata[ptr].mSize * ((dim > 0) ? dim : 1);
		memcpy(new_ptr, ptr, ____min(oldsize, size));
		if (size > oldsize)
			memset((char*)new_ptr + oldsize, 0, size - oldsize);
	}
	if (ptr)
		deallocate(ptr);
	return new_ptr;
}

void* Memory::allocate(const size_t size, const std::string owner, const std::string goal, const std::string file, const std::string function, const size_t line)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	if (!size)
		throw "Error: Zero memory allocation";
	METADATA metadata;
	void* result = alloc(size + HEAP_GUARD_AMOUNT);
	if (!result)
		throw "Error: Not enough memory in the heap";
	metadata.mFile = file;
	metadata.mFunction = function;
	metadata.mGoal = goal;
	metadata.mLine = line;
	metadata.mOriginalPtr = result;
	metadata.mOwner = owner;
	metadata.mSize = size;
	metadata.mArrayDimension = 0;
	mMetadata[result] = metadata;
	if (mDiagStarted)
	{
		mDiagnostic[result] = metadata;
	}
	mTotalSize += size;
	return result;
}

void Memory::deallocate(void* ptr)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	if (mMetadata.find(ptr) == mMetadata.end())
		throw "Error: attempt to deallocate non-allocated memory";
	mTotalSize -= mMetadata[ptr].mSize;
	mMetadata.erase(mMetadata.find(ptr));
	if (mDiagnostic.find(ptr) != mDiagnostic.end())
	{
		mDiagnostic.erase(ptr);
	}
	dealloc(ptr);
}

void* Memory::allocate_array(const size_t size, const size_t dim, const std::string owner, const std::string goal, const std::string file, const std::string function, const size_t line)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	if (!size)
		throw "Error: Zero memory allocation";
	METADATA metadata;
	void* result = alloc(size * dim);
	if (!result)
		throw "Error: Not enough memory in the heap";
	metadata.mFile = file;
	metadata.mFunction = function;
	metadata.mGoal = goal;
	metadata.mLine = line;
	metadata.mOriginalPtr = result;
	metadata.mOwner = owner;
	metadata.mSize = size;
	metadata.mArrayDimension = dim;
	mMetadata[result] = metadata;
	if (mDiagStarted)
	{
		mDiagnostic[result] = metadata;
	}
	mTotalSize += size * dim;
	return result;
}

void Memory::deallocate_array(void* ptr)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	if (mMetadata.find(ptr) == mMetadata.end())
		throw "Error: attempt to deallocate non-allocated memory";
	mTotalSize -= mMetadata[ptr].mSize * mMetadata[ptr].mArrayDimension;
	mMetadata.erase(mMetadata.find(ptr));
	if (mDiagnostic.find(ptr) != mDiagnostic.end())
	{
		mDiagnostic.erase(ptr);
	}
	dealloc(ptr);
}

size_t Memory::get_dimension(void* ptr)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	if (mMetadata.find(ptr) == mMetadata.end())
		throw "Error: bad dimension request";
	return mMetadata[ptr].mArrayDimension;
}

void Memory::report(const std::string file, const bool csv)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	std::ofstream f;
	f.open(file.c_str(), std::ios::binary);
	if (f.good() && !f.bad())
		if (csv)
			reportCSV(f, mMetadata, mTotalSize);
		else
			report(f, mMetadata, mTotalSize);
	f.close();
}
			
void Memory::report(std::ofstream& file, std::map<void*, METADATA> metadata, const unsigned long long total)
{
	file << "Totally used dynamic memory: " << total << " in " << metadata.size() << " blocks."<< std::endl;
	for (std::map<void*, METADATA>::iterator i = metadata.begin(); i != metadata.end(); i++)
	{
		file << "Object " << i->second.mOwner << " uses " << i->second.mSize * (i->second.mArrayDimension ? i->second.mArrayDimension : 1) <<
			" bytes for " << i->second.mGoal << " at address " << i->second.mOriginalPtr << " allocated in file " << i->second.mFile <<
			" in function " << i->second.mFunction << " at line " << i->second.mLine << std::endl;
	}
}

void Memory::reportCSV(std::ofstream& file, std::map<void*, METADATA> metadata, const unsigned long long)
{
	for (std::map<void*, METADATA>::iterator i = metadata.begin(); i != metadata.end(); i++)
	{
		file << i->second.mOwner << "," << i->second.mSize * (i->second.mArrayDimension ? i->second.mArrayDimension : 1) <<
			"," << i->second.mGoal << "," << i->second.mOriginalPtr << "," << i->second.mFile <<
			"," << i->second.mFunction << "," << i->second.mLine << std::endl;
	}
}

void Memory::start_diagnostic()
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	mDiagStarted = true;
	mDiagnostic.clear();
}

void Memory::stop_diagnostic()
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	mDiagStarted = false;
}

void Memory::report_diagnostic(const std::string file, const bool csv)
{
	if (!MetadataLock)
		MetadataLock = new mutex;
	scoped_lock lock(*MetadataLock);
	unsigned long long total = 0LL;
	for (std::map<void*, METADATA>::iterator i = mDiagnostic.begin(); i != mDiagnostic.end(); i++)
		if (i->second.mArrayDimension)
			total += i->second.mArrayDimension * i->second.mSize;
		else
			total += i->second.mSize;
	std::ofstream f;
	f.open(file.c_str(), std::ios::binary);
	if (f.good() && !f.bad())
		if (csv)
			reportCSV(f, mDiagnostic, total);
		else
			report(f, mDiagnostic, total);
	f.close();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

EasyHeap::EasyHeap(const size_t size, const size_t start_guard_size,
	const size_t end_guard_size, const bool fill)
	: mTotalSize(size), mCurrentSize(0), mStartGuard(start_guard_size), mEndGuard(end_guard_size), mPatternFill(fill)
{
	mMemory = new char[size];
	if (mPatternFill)
		memset(mMemory, 0, size);
	size_t* header = (size_t*)mMemory;
	header[0] = size;
	header[1] = 0;
	mMemory[2 * sizeof(size_t)] = 0x00;
	mCurrentSize = 2 * sizeof(size_t) + 1;
	mLock = new mutex;
}

EasyHeap::~EasyHeap()
{
	delete mMemory;
	delete (mutex*)mLock;
}

void* EasyHeap::alloc(const size_t size)
{
	scoped_lock lock(*(mutex*)mLock);
	const size_t header_size = 2 * sizeof(size_t) + 1;
	const size_t overhead_size = header_size + mStartGuard + mEndGuard;
	const size_t block_size = size + overhead_size;
	if (mTotalSize - mCurrentSize < block_size)
		return NULL;
	char* ptr = mMemory;
	size_t* header = (size_t*)ptr;
	char* prev = ptr;
	while (ptr[2 * sizeof(size_t)] || (((size_t*) ptr)[0] < block_size))
	{
		prev = ptr;
		ptr += *header;
		header = (size_t*)ptr;
		if (ptr > mMemory + mTotalSize - block_size)
			return NULL;
	}
	char* terminator = ptr + block_size;
	((size_t*)terminator)[0] = header[0] - block_size;
	((size_t*)terminator)[1] = block_size;
	terminator[2 * sizeof(size_t)] = 0x00;
	header[0] = block_size;
	header[1] = ptr - prev;
	ptr[2 * sizeof(size_t)] = (char) 0x80;
	if (mPatternFill)
	{
		memset(ptr + 2 * sizeof(size_t) + 1, 0x55, mStartGuard);
		memset(ptr + 2 * sizeof(size_t) + 1 + mStartGuard + size, 0xAA, mEndGuard);
		memset(ptr + 2 * sizeof(size_t) + 1 + mStartGuard, 0xFE, size);
	}
	mCurrentSize += block_size;
	return ptr + header_size + mStartGuard;
}

void EasyHeap::free(void* ptr)
{
	scoped_lock lock(*(mutex*)mLock);
	const size_t header_size = 2 * sizeof(size_t) + 1;
	char* header_ptr = (char*)ptr - header_size - mStartGuard;
	size_t* header = (size_t*) header_ptr;
	char* start_ptr = header_ptr;
	char* end_ptr = start_ptr;
	mCurrentSize -= *header;
	header_ptr[2 * sizeof(size_t)] = 0x00;
	while (header_ptr + *header < mMemory + mTotalSize)
	{
		header_ptr += *header;
		end_ptr = header_ptr;
		header = (size_t*)header_ptr;
		if (header_ptr[2 * sizeof(size_t)] == 0x00)
		{
			((size_t*)start_ptr)[0] += *header;
			char* next_head = start_ptr + ((size_t*)start_ptr)[0];
			((size_t*)next_head)[1] = ((size_t*)start_ptr)[0];
		}
		else
			break;
	}
	header_ptr = start_ptr;
	header = (size_t*)header_ptr;
	while (header_ptr > mMemory)
	{
		header_ptr -= header[1];
		if (header_ptr[2 * sizeof(size_t)] == 0x00)
		{
			header = (size_t*)header_ptr;
			header[0] += *(size_t*)start_ptr;
			((size_t*)end_ptr)[1] = end_ptr - header_ptr;
			start_ptr = header_ptr;
		}
		else
			break;
	}
	if (mPatternFill)
		memset(start_ptr + header_size, 0xEF, *(size_t*)start_ptr - header_size);
}

/////////////////////////////////////////////////////////////////

SlottedHeap::SlottedHeap(const size_t slot_size, const size_t slot_amount) : 
	mSlotSize(slot_size)
{
	mLock = new mutex;
	mTotalSize = slot_size * slot_amount;
	mCurrentSize = 0;
	mAllocationTable = new bool[slot_amount];
	mMemory = new char[mTotalSize];
	for (size_t i = 0; i < slot_amount; i++)
		mAllocationTable[i] = false;
}

SlottedHeap::~SlottedHeap()
{
	delete mMemory;
	delete mAllocationTable;
	delete (mutex*)mLock;
}

void* SlottedHeap::alloc(const size_t size)
{
	scoped_lock lock(*(mutex*)mLock);
	if (mTotalSize < size + mCurrentSize)
		return NULL;
	if (size > mSlotSize)
		return NULL;
	for (size_t i = 0; i < mTotalSize / mSlotSize; i++)
	{
		if (mAllocationTable[i] == false)
		{
			mAllocationTable[i] = true;
			return mMemory + i * mSlotSize;
		}
	}
	return NULL;
}

void SlottedHeap::free(void* ptr)
{
	scoped_lock lock(*(mutex*)mLock);
	size_t slot = ((char*)ptr - mMemory) / mSlotSize;
	mAllocationTable[slot] = false;
}

/////////////////////////////////////////////////////////////////////////////////////

ChainHeap::ChainHeap()
{
	mLock = new mutex;
}

ChainHeap::~ChainHeap()
{
	delete (mutex*)mLock;
}


void* ChainHeap::alloc(const size_t size) 
{
	scoped_lock lock(*(mutex*)mLock);
	for (std::list<IHeap*>::iterator i = mHeaps.begin(); i != mHeaps.end(); i++)
	{
		void* result = (*i)->alloc(size);
		if (result)
		{
			mPtrMaps[result] = *i;
			return result;
		}
	}
	return NULL;
}

void ChainHeap::free(void* ptr)
{
	scoped_lock lock(*(mutex*)mLock);
	std::map<void*, IHeap*>::iterator f = mPtrMaps.find(ptr);
	if (f != mPtrMaps.end())
	{
		f->second->free(ptr);
		mPtrMaps.erase(f);
	}
}
