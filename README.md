Common components library for cross-platform software development         
(C) Componentality Oy, 2008 - 2017                                        
Simple HTTP server for embedded use and integration to your C++ software  

(See LICENSE.TXT for MIT license for this software component)

Being compiled as is (in Windows or Linux) this HTTP server provides access to
static pre-loaded HTML and non-HTML documents. Server supports multi-threaded
HTTP requests processing, single shot and keep alive sessions.
You can also define your own resource providers by inheritance and creating 
instances of HTTPResourceProvider class to make a specific behavior as a
reaction to selected types of requested URLs.

The server is designed mostly for integration to an application software and
provide web-based access to it's core functionality. The simplest way to run
it is to add two lines to your code:
	CST::HTTP::HTTPServer server(port);
	CST::HTTP::FileAccessProvider file_provider(server, path);
You can instantiate more providers to process non-file based HTTP requests.

All comments and questions shall be addressed to dev@componentality.com and
will be processed with a regular priority. We're sorry if we can't rapidly
react on any input: we are commercial company with some obligations for our
customers and partners.
Sharing of this software component is our charity for engineering community,
we can't take any responsibility for the results of it's use as well as
guarantee bug fixes or improvements.

But your inputs and changes are always welcome.